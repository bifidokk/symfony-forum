<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: danilka
 * Date: 08.08.17
 * Time: 14:56.
 */

namespace ForumBundle\Repository;

use Tests\ForumBundle\ForumTestCase;

class ForumRepositoryTest extends ForumTestCase
{
    public function testGetAllForumsWithCategories()
    {
        $forumsFound = $this->getForumManager()->getAllForumsWithCategories();

        // total 5 forums
        $this->assertCount(5, $forumsFound);
    }

    public function testGetForumsFromCategory()
    {
        $forumsFound = $this->getForumManager()->getAllForumsWithCategories($id = 1);

        $this->assertInternalType('array', $forumsFound);
        $this->assertGreaterThan(0, \count($forumsFound));
    }
}
