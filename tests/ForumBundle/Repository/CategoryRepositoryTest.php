<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: danilka
 * Date: 08.08.17
 * Time: 14:41.
 */

namespace ForumBundle\Repository;

use Tests\ForumBundle\ForumTestCase;

class CategoryRepositoryTest extends ForumTestCase
{
    public function testFindAllCategoriesWithForums()
    {
        $categoriesFound = $this->getCategoryManager()->findAllWithForums();

        // 3 categories
        $this->assertCount(3, $categoriesFound);
    }
}
