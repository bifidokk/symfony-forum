<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: danilka
 * Date: 09.08.17
 * Time: 21:35.
 */

namespace ForumBundle\Repository;

use Doctrine\ORM\Query;
use ForumBundle\Entity\Post;
use Tests\ForumBundle\ForumTestCase;

class PostRepositoryTest extends ForumTestCase
{
    public function testCountPostsForTopicById()
    {
        $countPosts = $this->getPostManager()->countPostsForTopicById(1);

        $this->assertGreaterThan(0, $countPosts);
    }

    public function testGetPostsForTopicById()
    {
        $foundPosts = $this->getPostManager()->getPostsForTopicById(1);

        $this->assertInternalType('array', $foundPosts);
        $this->assertGreaterThan(0, \count($foundPosts));

        /** @var Post $post */
        $post = $foundPosts[0];
        $this->assertInstanceOf(Post::class, $post);
        $this->assertObjectHasAttribute('id', $post);
        $this->assertNotNull($post->getId());
    }

    public function testGetPostsQueryForTopicById()
    {
        $query = $this->getPostManager()->getPostsQueryForTopicById(1);

        $this->assertInstanceOf(Query::class, $query);
    }

    public function testGetLastPostForTopicById()
    {
        $post = $this->getPostManager()->getLastPostForTopicById(1);

        $this->assertInstanceOf(Post::class, $post);
        $this->assertObjectHasAttribute('id', $post);
        $this->assertNotNull($post->getId());
    }

    public function testGetFirstPostForTopicById()
    {
        $post = $this->getPostManager()->getFirstPostForTopicById(1);

        $this->assertInstanceOf(Post::class, $post);
        $this->assertObjectHasAttribute('id', $post);
        $this->assertNotNull($post->getId());
    }

    public function testGetLastPostForForumById()
    {
        $post = $this->getPostManager()->getLastPostForForumById(1);

        $this->assertInstanceOf(Post::class, $post);
        $this->assertObjectHasAttribute('id', $post);
        $this->assertNotNull($post->getId());
    }

    public function testGetCountPostsBeforeCurrentPost()
    {
        $post = $this->getPostManager()->getLastPostForTopicById(1);
        $count = $this->getPostManager()->getCountPostsBeforeCurrentPost($post);

        $this->assertInternalType('array', $count);
        $this->assertArrayHasKey('total', $count);
        $this->assertGreaterThan(0, $count['total']);
    }
}
