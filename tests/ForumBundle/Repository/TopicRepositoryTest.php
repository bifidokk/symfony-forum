<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: danilka
 * Date: 08.08.17
 * Time: 17:20.
 */

namespace ForumBundle\Repository;

use ForumBundle\Entity\Topic;
use Tests\ForumBundle\ForumTestCase;

class TopicRepositoryTest extends ForumTestCase
{
    public function testGetTopicAndPostCountForForumById()
    {
        $result = $this->getTopicManager()->getTopicAndPostCountForForumById(1);

        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('postCount', $result);
        $this->assertArrayHasKey('topicCount', $result);
        $this->assertGreaterThanOrEqual(0, $result['topicCount']);
        $this->assertGreaterThan(0, $result['postCount']);
    }

    public function testFindAllTopicsByForumId()
    {
        $topicsFound = $this->getTopicManager()->findAllTopicsByForumId(1);

        $this->assertInternalType('array', $topicsFound);
        $this->assertGreaterThan(0, \count($topicsFound));
    }

    public function testFindAllTopicsByTagId()
    {
        $topicsFound = $this->getTopicManager()->findAllTopicsByTagId(1);

        $this->assertInternalType('array', $topicsFound);
        $this->assertGreaterThan(0, \count($topicsFound));
    }

    public function testGetLastTopicForForumById()
    {
        $topicFound = $this->getTopicManager()->getLastTopicForForumById(1);
        $this->assertInstanceOf(Topic::class, $topicFound);
        $this->assertObjectHasAttribute('id', $topicFound);
        $this->assertNotNull($topicFound->getId());
    }
}
