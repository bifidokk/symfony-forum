<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: danilka
 * Date: 01.03.17
 * Time: 19:34.
 */

namespace Tests\ForumBundle;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use ForumBundle\DataFixtures\ORM\LoadFixtures;
use ForumBundle\Entity\User;
use ForumBundle\Handler\StatsUpdater;
use ForumBundle\Manager\CategoryManager;
use ForumBundle\Manager\ForumManager;
use ForumBundle\Manager\PostManager;
use ForumBundle\Manager\TopicManager;
use ForumBundle\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class ForumTestCase extends WebTestCase
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var StatsUpdater
     */
    protected $statsUpdater;

    /**
     * @var TokenGeneratorInterface
     */
    protected $tokenGenerator;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var CategoryManager
     */
    protected $categoryManager;

    /**
     * @var ForumManager
     */
    protected $forumManager;

    /**
     * @var TopicManager
     */
    protected $topicManager;

    /**
     * @var PostManager
     */
    protected $postManager;

    public static function setUpBeforeClass()
    {
    }

    protected function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->container = static::$kernel->getContainer();
        $this->em = $this->container
            ->get('doctrine')
            ->getManager()
        ;

        $this->statsUpdater = static::$kernel->getContainer()->get('stats_updater');
        $this->tokenGenerator = static::$kernel->getContainer()->get('security.csrf.token_generator');

        /*$this->em->getConnection()->query(sprintf('SET FOREIGN_KEY_CHECKS=0'));
        $purger = new ORMPurger($this->em);
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $purger->purge();
        $this->em->getConnection()->query(sprintf('SET FOREIGN_KEY_CHECKS=1'));

        $fixture = new LoadFixtures($this->statsUpdater);
        $fixture->load($this->em);*/

        /* @var UserManager */

        //$userManager = $this->container->get('forum.user_manager');
        /* @var User */
        /*$user = $userManager->createUser();
        $user->setName('danilka');
        $user->setEmail('bifidokk@gmail.com');
        $user->setPlainPassword('123456');
        $user->setEnabled(true);

        $userManager->updateUser($user);*/
    }

    /**
     * @param \Exception $e
     *
     * @throws \Exception
     */
    protected function onNotSuccessfulTest(\Exception $e)
    {
        throw $e;
    }

    /**
     * @param $id
     *
     * @return object
     */
    protected function getService($id)
    {
        return self::$kernel->getContainer()
            ->get($id);
    }

    /**
     * @return CategoryManager|object
     */
    protected function getCategoryManager()
    {
        if (null === $this->categoryManager) {
            $this->categoryManager = $this->container->get('category_manager');
        }

        return $this->categoryManager;
    }

    /**
     * @return ForumManager|object
     */
    protected function getForumManager()
    {
        if (null === $this->forumManager) {
            $this->forumManager = $this->container->get('forum_manager');
        }

        return $this->forumManager;
    }

    /**
     * @return TopicManager|object
     */
    protected function getTopicManager()
    {
        if (null === $this->topicManager) {
            $this->topicManager = $this->container->get('topic_manager');
        }

        return $this->topicManager;
    }

    protected function getPostManager()
    {
        if (null === $this->postManager) {
            $this->postManager = $this->container->get('post_manager');
        }

        return $this->postManager;
    }
}
