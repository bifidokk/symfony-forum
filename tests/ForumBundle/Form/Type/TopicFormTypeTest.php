<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: danilka
 * Date: 11.08.17
 * Time: 10:32.
 */

namespace ForumBundle\Form\Type;

use Doctrine\Common\Collections\ArrayCollection;
use ForumBundle\Entity\Post;
use ForumBundle\Entity\Topic;
use ForumBundle\Form\DataTransformer\TagToStringTransformer;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TopicFormTypeTest extends TypeTestCase
{
    private $transformer;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    protected function setUp()
    {
        $this->transformer = $this->getMockBuilder(TagToStringTransformer::class)
            ->disableOriginalConstructor()
            ->getMock();

        parent::setUp();
    }

    public function testSubmitValidData()
    {
        $formData = [
            'title' => 'test',
            'description' => 'test',
            'firstPost' => 'test',
            'tags' => new ArrayCollection(),
        ];

        $topic = new Topic();
        $topic->setTitle('test');
        $topic->setDescription('test');
        $topic->setFirstPost(
            $this->getMockBuilder(Post::class)
                ->getMock()
        );

        $form = $this->factory->create(TopicFormType::class, $topic);

        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertTrue($form->isValid());
        $this->assertEquals($topic, $form->getData());

        $view = $form->createView();
        $children = $view->children;

        foreach (\array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }

    protected function getExtensions()
    {
        $this->validator = $this->getMock(ValidatorInterface::class);
        $this->validator
            ->method('validate')
            ->will($this->returnValue(new ConstraintViolationList()));
        $this->validator
            ->method('getMetadataFor')
            ->will($this->returnValue(new ClassMetadata(Form::class)));

        $type = new TopicFormType($this->transformer);

        return [
            new PreloadedExtension([$type], []),
            new ValidatorExtension($this->validator),
        ];
    }
}
