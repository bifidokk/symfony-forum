<?php

declare(strict_types=1);

namespace Tests\ForumBundle\Controller;

use Tests\ForumBundle\ForumTestCase;

class DefaultControllerTest extends ForumTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertContains('Форум', $client->getResponse()->getContent());
    }
}
