<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: danilka
 * Date: 01.03.17
 * Time: 19:32.
 */

namespace Tests\ForumBundle\Controller;

use ForumBundle\Entity\Category;
use Tests\ForumBundle\ForumTestCase;

class CategoryControllerTest extends ForumTestCase
{
    protected function setUp()
    {
        parent::setUp();
    }

    public function testShow()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $categories = $crawler->filterXPath('//a[contains(@href, "/category/")]')
            ->evaluate('substring-after(@href, "/category/")');

        $forums = $crawler->filterXPath('//a[contains(@href, "/forum/")]')
            ->evaluate('substring-after(@href, "/forum/")');

        $this->assertEquals(3, \count($categories));
        $this->assertEquals(5, \count($forums));
    }

    public function testShowCategory()
    {
        $category = $this->getCategoryManager()->getCategory(1);
        $this->assertInstanceOf(Category::class, $category);
        $this->assertObjectHasAttribute('id', $category);

        $client = static::createClient();
        $crawler = $client->request('GET', '/category/'.$category->getId());

        $this->assertGreaterThan(
            0,
            $crawler->filter('h3:contains("category1")')->count()
        );

        $forums = $crawler->filterXPath('//a[contains(@href, "/forum/")]')
            ->evaluate('substring-after(@href, "/forum/")');

        $this->assertGreaterThan(0, \count($forums));
    }
}
