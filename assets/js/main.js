"use strict";

import Vue from 'vue'
import my_form from './public/test/Form.vue'
import user_edit_form from './public/user/UserEditForm.vue'

Vue.config.async = false;

Vue.component('my_form', my_form);
Vue.component('user_edit_form', user_edit_form);

new Vue({
    el: '#app'
});