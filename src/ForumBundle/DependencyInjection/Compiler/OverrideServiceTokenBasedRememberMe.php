<?php

declare(strict_types=1);

namespace ForumBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class OverrideServiceTokenBasedRememberMe.
 */
class OverrideServiceTokenBasedRememberMe implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('security.authentication.rememberme.services.simplehash');
        $definition->setClass('ForumBundle\Security\Http\RememberMe\TokenRememberMeServices');
    }
}
