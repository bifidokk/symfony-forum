<?php

declare(strict_types=1);

namespace ForumBundle\GraphQL\Type;

class DateTimeType
{
    public static function serialize(\DateTime $value)
    {
        return $value->format('Y-m-d H:i:s');
    }

    public static function parseValue($value)
    {
        return new \DateTime($value);
    }

    public static function parseLiteral($valueNode)
    {
        return new \DateTime($valueNode->value);
    }
}
