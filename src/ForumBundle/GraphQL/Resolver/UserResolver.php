<?php

declare(strict_types=1);

namespace ForumBundle\GraphQL\Resolver;

use Doctrine\ORM\EntityManager;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;

class UserResolver implements ResolverInterface, AliasedInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function findOne(Argument $args)
    {
        $user = $this->em->getRepository('ForumBundle:User')->find($args['id']);

        return $user;
    }

    public function find(Argument $args)
    {
        $users = $this->em->getRepository('ForumBundle:User')->findBy([], ['id' => 'asc'], $args['limit'], $args['offset']);

        return $users;
    }

    public static function getAliases()
    {
        return [
            'resolve' => 'User',
        ];
    }
}
