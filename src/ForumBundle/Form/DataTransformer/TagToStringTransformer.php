<?php

declare(strict_types=1);

namespace ForumBundle\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use ForumBundle\Entity\Tag;
use Symfony\Component\Form\DataTransformerInterface;

class TagToStringTransformer implements DataTransformerInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($tags)
    {
        $newArray = [];

        if (!($tags instanceof PersistentCollection)) {
            return '';
        }

        foreach ($tags as $key => $value) {
            if ($value instanceof Tag) {
                $newArray[] = $value->getName();
            }
        }

        return \implode(', ', $newArray);
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($tags)
    {
        $newArray = [];

        if (!$tags) {
            return new ArrayCollection();
        }

        $tags = \explode(',', $tags);
        foreach ($tags as $name) {
            $name = \trim($name);
            // TODO можно запилить с IN
            if ($name) {
                $tag = $this->em
                    ->getRepository('ForumBundle:Tag')
                    ->findOneBy(['name' => $name])
                ;

                if (\is_null($tag)) {
                    $tag = new Tag();
                    $tag->setName($name);
                }

                $newArray[] = $tag;
            }
        }

        if (!empty($newArray)) {
            return new ArrayCollection($newArray);
        }

        return new ArrayCollection();
    }
}
