<?php

declare(strict_types=1);

namespace ForumBundle\Form\Handler;

use ForumBundle\Entity\Post;
use ForumBundle\Event\UserEvents;
use ForumBundle\Event\UserTopicEvent;
use ForumBundle\Form\Type\PostDeleteFormType;
use ForumBundle\Manager\PostManager;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher;

class PostDeleteFormHandler extends BaseFormHandler
{
    private $form;
    /** * @var $post Post */
    private $post;
    private $factory;
    private $postManager;
    private $dispatcher;

    public function __construct(FormFactory $factory, PostManager $postManager, TraceableEventDispatcher $dispatcher)
    {
        $this->factory = $factory;
        $this->postManager = $postManager;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Post $post
     */
    public function setPost(Post $post)
    {
        if (!$post instanceof Post) {
            throw new \InvalidArgumentException('Not instance of Post');
        }

        $this->post = $post;
    }

    /**
     * @return \Symfony\Component\Form\FormInterface
     */
    public function getForm()
    {
        if (null === $this->form) {
            $this->form = $this->factory->create(PostDeleteFormType::class, $this->post);
        }

        return $this->form;
    }

    public function process()
    {
        $form = $this->getForm();
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->onSuccess($this->post);

            return true;
        }

        return false;
    }

    protected function onSuccess(Post $post)
    {
        $topic = $this->post->getTopic();
        $this->postManager->deletePost($post);

        $event = new UserTopicEvent($this->request, $topic);
        $this->dispatcher->dispatch(UserEvents::USER_POST_DELETED, $event);
    }
}
