<?php

declare(strict_types=1);

namespace ForumBundle\Form\Handler;

use ForumBundle\Entity\Post;
use ForumBundle\Entity\Topic;
use ForumBundle\Entity\User;
use ForumBundle\Event\UserEvents;
use ForumBundle\Event\UserPostEvent;
use ForumBundle\Form\Type\PostFormType;
use ForumBundle\Manager\PostManager;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcher;
use Symfony\Component\Form\FormFactory;

class PostCreateFormHandler extends BaseFormHandler
{
    private $factory;
    private $postManager;
    /** @var $post Post */
    private $post;
    private $form;
    private $dispatcher;

    public function __construct(FormFactory $factory, PostManager $postManager, TraceableEventDispatcher $dispatcher)
    {
        $this->factory = $factory;
        $this->postManager = $postManager;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Post $post
     */
    public function setPost(Post $post)
    {
        if (!$post instanceof Post) {
            throw new \InvalidArgumentException('Not instance of Post');
        }

        $this->post = $post;
    }

    /**
     * @return \Symfony\Component\Form\FormInterface
     */
    public function getForm()
    {
        if (null === $this->form) {
            $this->form = $this->factory->create(PostFormType::class, $this->post);
        }

        $this->form->setData($this->post);

        return $this->form;
    }

    /**
     * @param Topic $topic
     * @param User  $user
     *
     * @return bool
     */
    public function process(Topic $topic, User $user)
    {
        $form = $this->getForm();
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->post->setAuthor($user);
            $this->post->setTopic($topic);

            $this->onSuccess($this->post);

            return true;
        }

        return false;
    }

    protected function onSuccess(Post $post)
    {
        $this->postManager->savePost($post);

        $event = new UserPostEvent($this->request, $post);
        $this->dispatcher->dispatch(UserEvents::USER_POST_ADDED, $event);
    }
}
