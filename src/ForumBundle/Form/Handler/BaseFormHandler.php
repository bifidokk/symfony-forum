<?php

declare(strict_types=1);

namespace ForumBundle\Form\Handler;

use Symfony\Component\HttpFoundation\Request;

class BaseFormHandler
{
    protected $request;

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }
}
