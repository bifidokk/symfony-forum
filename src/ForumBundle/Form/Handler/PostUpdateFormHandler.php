<?php

declare(strict_types=1);

namespace ForumBundle\Form\Handler;

use ForumBundle\Entity\Post;
use ForumBundle\Form\Type\PostFormType;
use ForumBundle\Manager\PostManager;
use Symfony\Component\Form\FormFactory;

class PostUpdateFormHandler extends BaseFormHandler
{
    private $post;
    private $form;
    private $factory;
    private $postManager;

    public function __construct(FormFactory $factory, PostManager $postManager)
    {
        $this->factory = $factory;
        $this->postManager = $postManager;
    }

    /**
     * @param Post $post
     */
    public function setPost(Post $post)
    {
        if (!$post instanceof Post) {
            throw new \InvalidArgumentException('Not instance of Post');
        }

        $this->post = $post;
    }

    /**
     * @return bool
     */
    public function process()
    {
        $form = $this->getForm();
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $this->onSuccess($formData);

            return true;
        }

        return false;
    }

    /**
     * @return \Symfony\Component\Form\FormInterface
     */
    public function getForm()
    {
        if (null === $this->form) {
            $this->form = $this->factory->create(PostFormType::class, $this->post);
        }

        $this->form->setData($this->post);

        return $this->form;
    }

    /**
     * @param Post $post
     */
    protected function onSuccess(Post $post)
    {
        $this->postManager->savePost($post);
    }
}
