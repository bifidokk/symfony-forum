<?php

declare(strict_types=1);

namespace ForumBundle\Form\Handler;

use ForumBundle\Entity\Forum;
use ForumBundle\Entity\Topic;
use ForumBundle\Event\UserEvents;
use ForumBundle\Event\UserTopicEvent;
use ForumBundle\Form\Type\TopicMoveFormType;
use ForumBundle\Manager\TopicManager;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher;

class TopicMoveFormHandler extends BaseFormHandler
{
    private $form;
    /** @var $topic Topic */
    private $topic;
    private $factory;
    private $topicManager;
    private $dispatcher;

    public function __construct(FormFactory $factory, TopicManager $topicManager, TraceableEventDispatcher $dispatcher)
    {
        $this->factory = $factory;
        $this->topicManager = $topicManager;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Topic $topic
     */
    public function setTopic(Topic $topic)
    {
        if (!$topic instanceof Topic) {
            throw new \InvalidArgumentException('Not instance of Topic');
        }

        $this->topic = $topic;
    }

    /**
     * @return \Symfony\Component\Form\FormInterface
     */
    public function getForm()
    {
        if (null === $this->form) {
            $this->form = $this->factory->create(TopicMoveFormType::class, $this->topic);
        }

        return $this->form;
    }

    /**
     * @return bool
     */
    public function process()
    {
        $oldForum = $this->topic->getForum();

        $form = $this->getForm();
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->onSuccess($this->topic, $oldForum);

            return true;
        }

        return false;
    }

    protected function onSuccess(Topic $topic, Forum $oldForum)
    {
        $this->topicManager->saveTopic($topic);

        $event = new UserTopicEvent($this->request, $topic);
        $event->setOldForum($oldForum);

        $this->dispatcher->dispatch(UserEvents::USER_TOPIC_MOVED, $event);
    }
}
