<?php

declare(strict_types=1);

namespace ForumBundle\Form\Handler;

use ForumBundle\Entity\Forum;
use ForumBundle\Entity\Topic;
use ForumBundle\Entity\User;
use ForumBundle\Event\UserEvents;
use ForumBundle\Event\UserPostEvent;
use ForumBundle\Form\Type\TopicFormType;
use ForumBundle\Manager\TopicManager;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher;

class TopicCreateFormHandler extends BaseFormHandler
{
    private $form;
    /** * @var $topic Topic */
    private $topic;
    private $factory;
    private $topicManager;
    private $dispatcher;

    public function __construct(FormFactory $factory, TopicManager $topicManager, TraceableEventDispatcher $dispatcher)
    {
        $this->factory = $factory;
        $this->topicManager = $topicManager;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Topic $topic
     */
    public function setTopic(Topic $topic)
    {
        if (!$topic instanceof Topic) {
            throw new \InvalidArgumentException('Not instance of Topic');
        }

        $this->topic = $topic;
    }

    /**
     * @return \Symfony\Component\Form\FormInterface
     */
    public function getForm()
    {
        if (null === $this->form) {
            $this->form = $this->factory->create(TopicFormType::class, $this->topic);
        }

        $this->form->setData($this->topic);

        return $this->form;
    }

    /**
     * @param Forum $forum
     * @param User  $user
     *
     * @return bool
     */
    public function process(Forum $forum, User $user)
    {
        $form = $this->getForm();
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->topic->setForum($forum);

            $this->topic->getFirstPost()->setAuthor($user);
            $this->topic->getFirstPost()->setTopic($this->topic);

            $this->onSuccess($this->topic);

            return true;
        }

        return false;
    }

    protected function onSuccess(Topic $topic)
    {
        $this->topicManager->saveTopic($topic);

        $event = new UserPostEvent($this->request, $topic->getFirstPost());
        $this->dispatcher->dispatch(UserEvents::USER_POST_ADDED, $event);
    }
}
