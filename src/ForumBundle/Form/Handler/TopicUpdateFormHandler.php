<?php

declare(strict_types=1);

namespace ForumBundle\Form\Handler;

use Doctrine\Common\Collections\ArrayCollection;
use ForumBundle\Entity\Post;
use ForumBundle\Entity\Tag;
use ForumBundle\Entity\Topic;
use ForumBundle\Form\Type\TopicFormType;
use ForumBundle\Manager\TagManager;
use ForumBundle\Manager\TopicManager;
use Symfony\Component\Form\FormFactory;

class TopicUpdateFormHandler extends BaseFormHandler
{
    /** @var $post Post */
    private $post;
    private $form;
    private $factory;
    private $topicManager;
    private $tagManager;
    private $originalTags;

    public function __construct(FormFactory $factory, TopicManager $topicManager, TagManager $tagManager)
    {
        $this->factory = $factory;
        $this->topicManager = $topicManager;
        $this->tagManager = $tagManager;
    }

    /**
     * @param Post $post
     */
    public function setPost(Post $post)
    {
        if (!$post instanceof Post) {
            throw new \InvalidArgumentException('Not instance of Post');
        }

        $this->post = $post;
    }

    /**
     * @return bool
     */
    public function process()
    {
        $form = $this->getForm();
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $this->onSuccess($formData);

            return true;
        }

        return false;
    }

    /**
     * @throws \Exception
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    public function getForm()
    {
        $topic = $this->post->getTopic();
        if (!$topic instanceof Topic) {
            throw new \Exception('Post must have a valid Topic in TopicUpdateFormHandler');
        }

        $this->originalTags = new ArrayCollection();

        foreach ($topic->getTags() as $tag) {
            $this->originalTags->add($tag);
        }

        if (null === $this->form) {
            $this->form = $this->factory->create(TopicFormType::class, $topic);
        }

        $this->form->setData($topic);

        return $this->form;
    }

    /**
     * @param Topic $topic
     */
    protected function onSuccess(Topic $topic)
    {
        $tags = $topic->getTags();

        /** @var Tag $tag */
        foreach ($this->originalTags as $tag) {
            if (false === $tags->contains($tag)) {
                $tag->getTopics()->removeElement($topic);
                $this->tagManager->saveTag($tag);
            }
        }

        $this->topicManager->saveTopic($topic);
    }
}
