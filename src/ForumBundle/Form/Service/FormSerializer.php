<?php

declare(strict_types=1);

namespace ForumBundle\Form\Service;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Serializer\SerializerInterface;

class FormSerializer
{
    private $factory;
    private $serializer;

    public function __construct(FormFactoryInterface $factory, SerializerInterface $serializer)
    {
        $this->factory = $factory;
        $this->serializer = $serializer;
    }

    /**
     * @param $formTypeName
     * @param $object
     *
     * @return string
     */
    public function serialize($formTypeName, $object)
    {
        $form = $this->factory->create($formTypeName, $object);
        $data = $this->transform($form);

        return $this->serializer->serialize($data, 'json');
    }

    /**
     * @param FormInterface $form
     *
     * @return array
     */
    private function transform(FormInterface $form)
    {
        $data = [];

        foreach ($form->createView()->children as $child) {
            $data[$child->vars['full_name']] = [
                'type' => $child->vars['block_prefixes'][1],
                'inputType' => $child->vars['block_prefixes'][1],
                'label' => $child->vars['label'],
                'model' => $child->vars['full_name'],
                'attr' => $child->vars['attr'],
            ];
        }

        return $data;
    }
}
