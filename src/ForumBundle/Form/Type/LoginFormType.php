<?php

declare(strict_types=1);

namespace ForumBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class LoginFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Электронная почта',
                'attr' => [
                    'maxlength' => 32,
                    'pattern' => '.{5,32}',
                    'required title' => 'от 5 до 32 символов',
                    'placeholder' => 'Электронная почта',
                ],
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 5, 'max' => 32]),
                    new Email(['message' => 'Вы должны ввести адрес электронной почты']),
                ],
            ])
            ->add('plainPassword', PasswordType::class, [
                'label' => 'Электронная почта',
                'attr' => [
                    'maxlength' => 32,
                    'pattern' => '.{3,32}',
                    'required title' => 'от 3 до 32 символов',
                    'placeholder' => 'Пароль',
                ],
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3, 'max' => 32]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => true,
            'csrf_field_name' => '_login_csrf_token',
            'csrf_token_id' => 'login',
            'intention' => 'login',
            'data_class' => 'ForumBundle\Entity\User',
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
