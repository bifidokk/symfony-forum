<?php

declare(strict_types=1);

namespace ForumBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class PostFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('post', TextareaType::class, [
                'label' => 'Текст сообщения',
                'attr' => [
                    'maxlength' => 1024,
                    'pattern' => '.{2,1024}',
                    'required title' => 'от 2 до 1024 символов',
                    'placeholder' => 'Текст сообщения',
                ],
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 2, 'max' => 1024]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => true,
            'csrf_field_name' => '_post_csrf_token',
            'csrf_token_id' => 'post',
            'intention' => 'post',
            'data_class' => 'ForumBundle\Entity\Post',
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
