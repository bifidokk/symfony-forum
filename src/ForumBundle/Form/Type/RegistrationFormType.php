<?php

declare(strict_types=1);

namespace ForumBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Имя',
                'attr' => [
                    'maxlength' => 32,
                    'pattern' => '.{5,32}',
                    'required title' => 'от 5 до 32 символов',
                    'placeholder' => 'Имя',
                ],
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 5, 'max' => 32]),
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => 'Электронная почта',
                'attr' => [
                    'maxlength' => 32,
                    'pattern' => '.{5,32}',
                    'required title' => 'от 5 до 32 символов',
                    'placeholder' => 'Электронная почта',
                ],
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 5, 'max' => 32]),
                    new Email(['message' => 'Вы должны ввести адрес электронной почты']),
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => [
                    'attr' => [
                        'maxlength' => 32,
                        'pattern' => '.{3,32}',
                        'required title' => 'от 3 до 32 символов',
                        'placeholder' => 'Пароль',
                    ],
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                        new Length(['min' => 3, 'max' => 32]),
                    ],
                ],
                'second_options' => [
                    'attr' => [
                        'maxlength' => 32,
                        'pattern' => '.{3,32}',
                        'required title' => 'от 3 до 32 символов',
                        'placeholder' => 'Повторите пароль',
                    ],
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                        new Length(['min' => 3, 'max' => 32]),
                    ],
                ],
                'invalid_message' => 'Вы неправильно повторили пароль.',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => true,
            'csrf_field_name' => '_registration_csrf_token',
            'csrf_token_id' => 'registration',
            'intention' => 'registration',
            'data_class' => 'ForumBundle\Entity\User',
            'validation_groups' => ['Default', 'Registration'],
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
