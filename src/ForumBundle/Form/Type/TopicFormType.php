<?php

declare(strict_types=1);

namespace ForumBundle\Form\Type;

use ForumBundle\Form\DataTransformer\TagToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class TopicFormType extends AbstractType
{
    private $transformer;

    public function __construct(TagToStringTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Название темы',
                'attr' => [
                    'maxlength' => 255,
                    'pattern' => '.{3,255}',
                    'required title' => 'от 3 до 255 символов',
                    'placeholder' => 'Название темы',
                ],
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3, 'max' => 255]),
                ],
            ])->add('description', TextType::class, [
                'label' => 'Описание темы',
                'attr' => [
                    'maxlength' => 255,
                    'pattern' => '.{3,255}',
                    'required title' => 'от 3 до 255 символов',
                    'placeholder' => 'Описание темы',
                ],
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3, 'max' => 255]),
                ],
            ])->add('firstPost', PostFormType::class)
            ->add('tags', TextType::class, [
                'label' => 'Теги',
                'attr' => [
                    'maxlength' => 255,
                    'placeholder' => 'Теги',
                ],
                'required' => false,
            ]);

        $builder->get('tags')
            ->addModelTransformer($this->transformer)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => true,
            'csrf_field_name' => '_topic_csrf_token',
            'csrf_token_id' => 'topic',
            'intention' => 'topic',
            'data_class' => 'ForumBundle\Entity\Topic',
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
