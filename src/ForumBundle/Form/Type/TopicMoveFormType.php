<?php

declare(strict_types=1);

namespace ForumBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use ForumBundle\Entity\Forum;
use ForumBundle\Entity\Topic;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class TopicMoveFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $currentForumId = 0;

        if ($options['data'] instanceof Topic) {
            $currentForumId = $options['data']->getForum()->getId();
        }
        $builder
            ->add('id', HiddenType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                ],
            ])->add('forum', EntityType::class, [
                'class' => 'ForumBundle\Entity\Forum',
                'query_builder' => function (EntityRepository $er) use ($currentForumId) {
                    return $er->createQueryBuilder('f')->select(['f', 'c'])
                        ->join('f.category', 'c', 'WITH')
                        ->where('f.id != :currentForumId')
                        ->setParameters([
                            'currentForumId' => $currentForumId,
                        ])
                    ;
                },
                'choice_label' => 'name',
                'placeholder' => 'Выберите раздел',
                'required' => true,
                'group_by' => function (Forum $forum) {
                    return $forum->getCategory()->getName();
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => true,
            'csrf_field_name' => '_topic_move_csrf_token',
            'csrf_token_id' => 'topic',
            'intention' => 'topic',
            'data_class' => 'ForumBundle\Entity\Topic',
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
