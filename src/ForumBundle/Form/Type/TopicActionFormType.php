<?php

declare(strict_types=1);

namespace ForumBundle\Form\Type;

use ForumBundle\Entity\Topic;
use ForumBundle\Form\ChoiceList\Loader\TopicActionsChoiceLoader;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class TopicActionFormType extends AbstractType
{
    private $loader;

    public function __construct(TopicActionsChoiceLoader $loader)
    {
        $this->loader = $loader;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /**
         * @var Topic
         */
        $topic = $options['topic'];

        $builder
            ->add('action', ChoiceType::class, [
                'label' => 'Действие с темой',
                'choice_loader' => new CallbackChoiceLoader(function () use ($topic) {
                    return $this->loader->getTopicActions($topic);
                }),

                'required' => true,
                'constraints' => [
                    new NotBlank(),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => true,
            'csrf_field_name' => '_topic_csrf_token',
            'csrf_token_id' => 'topic',
            'intention' => 'topic',
        ]);

        $resolver->setRequired('topic');
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
