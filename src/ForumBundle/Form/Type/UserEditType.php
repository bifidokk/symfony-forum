<?php

declare(strict_types=1);

namespace ForumBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Имя',
                'attr' => [
                    'maxlength' => 32,
                    'pattern' => '.{3,32}',
                    'required title' => 'от 3 до 32 символов',
                    'placeholder' => 'Электронная почта',
                ],
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3, 'max' => 32]),
                ],
            ])
            ->add('action', ChoiceType::class, [
                'label' => 'Выбрать',
                'choices' => [
                    'test' => 1,
                    'test2' => 2,
                ],
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'ForumBundle\DTO\UserEditData',
        ]);
    }

    public function getBlockPrefix()
    {
        return 'user_edit';
    }
}
