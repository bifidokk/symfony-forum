<?php

declare(strict_types=1);

namespace ForumBundle\Form\ChoiceList\Loader;

use ForumBundle\Entity\Topic;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class TopicActionsChoiceLoader
{
    private $availableChoices = [
        'delete' => 'Удалить',
        'sticky' => 'Прикрепить',
        'unsticky' => 'Открепить',
        'close' => 'Закрыть',
        'reopen' => 'Открыть',
        'move' => 'Перенести',
    ];

    private $actions = [];

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param Topic $topic
     *
     * @return array
     */
    public function getTopicActions(Topic $topic)
    {
        foreach ($this->availableChoices as $action => $choiceLabel) {
            if ($this->authorizationChecker->isGranted($action, $topic)) {
                $this->actions[$choiceLabel] = $action;
            }
        }

        return $this->actions;
    }
}
