<?php

declare(strict_types=1);

namespace ForumBundle\Form\ChoiceList\Loader;

class TopicActionsChoice
{
    const CHOICE_DELETE = 'delete';
    const CHOICE_STICKY = 'sticky';
    const CHOICE_UNSTICKY = 'unsticky';
    const CHOICE_CLOSE = 'close';
    const CHOICE_REOPEN = 'reopen';
    const CHOICE_MOVE = 'move';
}
