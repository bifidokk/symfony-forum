<?php

declare(strict_types=1);

namespace ForumBundle\Security;

use ForumBundle\Entity\User;
use ForumBundle\Manager\UserManager;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{
    protected $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    public function loadUserByUsername($username)
    {
        $userData = $this->userManager->findUserBy(['name' => $username]);

        if ($userData) {
            return $userData;
        }

        throw new UsernameNotFoundException(
            \sprintf('Пользователь с ником %s не существует.', $username)
        );
    }

    /**
     * @param $email
     *
     * @return mixed
     */
    public function loadUserByEmail($email)
    {
        $userData = $this->userManager->findUserByEmail($email);

        if ($userData) {
            return $userData;
        }

        throw new UsernameNotFoundException(
            \sprintf('Пользователь с email %s не существует.', $email)
        );
    }

    public function loadUserById($id)
    {
        $userData = $this->userManager->findUserBy(['id' => $id]);

        if ($userData) {
            return $userData;
        }

        throw new UsernameNotFoundException(
            \sprintf('Пользователь не существует.', $id)
        );
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                \sprintf('Instances of "%s" are not supported.', \get_class($user))
            );
        }

        return $this->loadUserById($user->getId());
    }

    public function supportsClass($class)
    {
        return 'ForumBundle\Entity\User' === $class;
    }
}
