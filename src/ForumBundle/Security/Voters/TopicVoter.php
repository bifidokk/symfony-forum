<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: danilka
 * Date: 31.07.17
 * Time: 16:57.
 */

namespace ForumBundle\Security\Voters;

use Doctrine\ORM\EntityManager;
use ForumBundle\Entity\Topic;
use ForumBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class TopicVoter extends Voter
{
    const REPLY = 'reply';
    const DELETE = 'delete';
    const STICKY = 'sticky';
    const UNSTICKY = 'unsticky';
    const CLOSE = 'close';
    const REOPEN = 'reopen';
    const MOVE = 'move';

    private $em;
    private $decisionManager;

    public function __construct(EntityManager $entityManager, AccessDecisionManagerInterface $decisionManager)
    {
        $this->em = $entityManager;
        $this->decisionManager = $decisionManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if (!\in_array($attribute, [
            self::REPLY,
            self::DELETE,
            self::STICKY,
            self::UNSTICKY,
            self::CLOSE,
            self::REOPEN,
            self::MOVE,
        ], true)) {
            return false;
        }

        if (!$subject instanceof Topic) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }

        /** @var Topic $topic */
        $topic = $subject;

        switch ($attribute) {
            case self::REPLY:
                return $this->canReply($token, $topic);
                break;
            case self::DELETE:
                return $this->canDelete($token);
                break;
            case self::STICKY:
                return $this->canSticky($token, $topic);
                break;
            case self::UNSTICKY:
                return $this->canUnsticky($token, $topic);
                break;
            case self::CLOSE:
                return $this->canClose($token, $topic);
                break;
            case self::REOPEN:
                return $this->canReopen($token, $topic);
                break;
            case self::MOVE:
                return $this->canMove($token);
                break;
        }
        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param TokenInterface $token
     * @param Topic          $topic
     *
     * @return bool
     */
    private function canReply(TokenInterface $token, Topic $topic)
    {
        if (!$this->decisionManager->decide($token, ['ROLE_USER'])) {
            return false;
        }

        if ($topic->getClosed()) {
            return false;
        }

        return true;
    }

    /**
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canDelete(TokenInterface $token)
    {
        if (!$this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return false;
        }

        return true;
    }

    /**
     * @param TokenInterface $token
     * @param Topic          $topic
     *
     * @return bool
     */
    private function canSticky(TokenInterface $token, Topic $topic)
    {
        if (!$this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return false;
        }

        if ($topic->getIsSticky()) {
            return false;
        }

        return true;
    }

    /**
     * @param TokenInterface $token
     * @param Topic          $topic
     *
     * @return bool
     */
    private function canUnsticky(TokenInterface $token, Topic $topic)
    {
        if (!$this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return false;
        }

        if (!$topic->getIsSticky()) {
            return false;
        }

        return true;
    }

    /**
     * @param TokenInterface $token
     * @param Topic          $topic
     *
     * @return bool
     */
    private function canClose(TokenInterface $token, Topic $topic)
    {
        if (!$this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return false;
        }

        if ($topic->getClosed()) {
            return false;
        }

        return true;
    }

    /**
     * @param TokenInterface $token
     * @param Topic          $topic
     *
     * @return bool
     */
    private function canReopen(TokenInterface $token, Topic $topic)
    {
        if (!$this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return false;
        }

        if (!$topic->getClosed()) {
            return false;
        }

        return true;
    }

    /**
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canMove(TokenInterface $token)
    {
        if (!$this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return false;
        }

        return true;
    }
}
