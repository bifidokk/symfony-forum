<?php

declare(strict_types=1);

namespace ForumBundle\Security\Voters;

use Doctrine\ORM\EntityManager;
use ForumBundle\Entity\Post;
use ForumBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PostVoter extends Voter
{
    const CREATE = 'create';
    const EDIT = 'edit';
    const DELETE = 'delete';

    private $em;
    private $decisionManager;

    public function __construct(EntityManager $entityManager, AccessDecisionManagerInterface $decisionManager)
    {
        $this->em = $entityManager;
        $this->decisionManager = $decisionManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if (!\in_array($attribute, [self::CREATE, self::EDIT, self::DELETE], true)) {
            return false;
        }

        if (!$subject instanceof Post) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }

        if (\in_array('ROLE_ADMIN', $user->getRoles(), true)) {
            return true;
        }

        $post = $subject;

        switch ($attribute) {
            case self::CREATE:
                return $this->canCreate($token);
                break;
            case self::EDIT:
                return $this->canEdit($token, $post, $user);
                break;
            case self::DELETE:
                return $this->canDelete($token, $post, $user);
                break;
        }
        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canCreate(TokenInterface $token)
    {
        if (!$this->decisionManager->decide($token, ['ROLE_USER'])) {
            return false;
        }

        return true;
    }

    /**
     * @param TokenInterface $token
     * @param Post           $post
     * @param User           $user
     *
     * @return bool
     */
    private function canEdit(TokenInterface $token, Post $post, User $user)
    {
        if (!$this->decisionManager->decide($token, ['ROLE_USER'])) {
            return false;
        }

        if ($post->getAuthor()->getId() !== $user->getId()) {
            return false;
        }

        return true;
    }

    /**
     * @param TokenInterface $token
     * @param Post           $post
     * @param User           $user
     *
     * @return bool
     */
    private function canDelete(TokenInterface $token, Post $post, User $user)
    {
        if (!$this->decisionManager->decide($token, ['ROLE_USER'])) {
            return false;
        }

        if ($post->getAuthor()->getId() !== $user->getId()) {
            return false;
        }

        return true;
    }
}
