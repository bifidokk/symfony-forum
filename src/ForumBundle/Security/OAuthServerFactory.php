<?php

declare(strict_types=1);

namespace ForumBundle\Security;

use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\CryptKey;
use League\OAuth2\Server\Grant\PasswordGrant;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;

class OAuthServerFactory
{
    /**
     * @var \League\OAuth2\Server\CryptKey
     */
    protected $privateKey;

    /**
     * @var string
     */
    protected $encryptionKey;

    /**
     * @var \League\OAuth2\Server\Repositories\ClientRepositoryInterface
     */
    private $clientRepository;

    /**
     * @var \League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface
     */
    private $accessTokenRepository;

    /**
     * @var \League\OAuth2\Server\Repositories\ScopeRepositoryInterface
     */
    private $scopeRepository;

    public function __construct(
        ClientRepositoryInterface $clientRepo,
        AccessTokenRepositoryInterface $accessTokenRepo,
        ScopeRepositoryInterface $scopeRepo,
        CryptKey $privateKey,
        string $encryptionKey
    ) {
        $this->clientRepository = $clientRepo;
        $this->accessTokenRepository = $accessTokenRepo;
        $this->scopeRepository = $scopeRepo;
        $this->privateKey = $privateKey;
        $this->encryptionKey = $encryptionKey;
    }

    public function buildAuthServer(
        UserRepositoryInterface $userRepository,
        RefreshTokenRepositoryInterface $refreshTokenRepository): AuthorizationServer
    {
        $server = new AuthorizationServer(
            $this->clientRepository,
            $this->accessTokenRepository,
            $this->scopeRepository,
            $this->privateKey,
            $this->encryptionKey
        );

        $grant = new PasswordGrant(
            $userRepository,
            $refreshTokenRepository
        );
        $grant->setRefreshTokenTTL(new \DateInterval('P1M')); // refresh tokens will expire after 1 month
        $server->enableGrantType(
            $grant,
            new \DateInterval('PT24H') // access tokens will expire after 24 hours
        );

        $grant = new RefreshTokenGrant($refreshTokenRepository);
        $grant->setRefreshTokenTTL(new \DateInterval('P1Y')); // new refresh tokens will expire after 1 year
        $server->enableGrantType(
            $grant,
            new \DateInterval('PT24H') // new access tokens will expire after an hour
        );

        return $server;
    }
}
