<?php

declare(strict_types=1);

namespace ForumBundle\Security;

use ForumBundle\Entity\User;
use ForumBundle\Form\Type\LoginFormType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class FormAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * Default message for authentication failure.
     *
     * @var string
     */
    private $failMessage = 'Некорректные данные';

    private $router;
    private $formFactory;
    private $encoderFactory;

    public function __construct(RouterInterface $router, FormFactory $formFactory, EncoderFactoryInterface $encoderFactory)
    {
        $this->router = $router;
        $this->formFactory = $formFactory;
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, new UsernameNotFoundException('Вы должны авторизоваться'));
        $url = $this->router->generate('login');

        return new RedirectResponse($url);
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(Request $request)
    {
        if ('/login_check' !== $request->getPathInfo() || !$request->isMethod('POST')) {
            return;
        }

        if (!$request->request->get('email') || !$request->request->get('plainPassword')) {
            throw new BadCredentialsException('Некорректные данные');
        }

        $user = new User();
        $loginForm = $this->formFactory->create(LoginFormType::class, $user);
        $loginForm->handleRequest($request);
        if (!$loginForm->isSubmitted() || !$loginForm->isValid()) {
            throw new BadCredentialsException((string) $loginForm->getErrors(true));
        }

        $email = $request->request->get('email');
        $plainPassword = $request->request->get('plainPassword');

        return [
            'email' => $email,
            'plainPassword' => $plainPassword,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        return $userProvider->loadUserByEmail($credentials['email']);
    }

    /**
     * {@inheritdoc}
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        $plainPassword = $credentials['plainPassword'];
        $encoder = $this->encoderFactory->getEncoder($user);

        if ($encoder->isPasswordValid($user->getPassword(), $plainPassword, $user->getSalt())) {
            return true;
        }

        throw new CustomUserMessageAuthenticationException($this->failMessage);
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
        $url = $this->router->generate('login');

        return new RedirectResponse($url);
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $url = $this->router->generate('forum_categories_list');

        return new RedirectResponse($url);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsRememberMe()
    {
        return true;
    }
}
