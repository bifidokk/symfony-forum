<?php

declare(strict_types=1);

namespace ForumBundle\Security;

use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResourceServer;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class OAuthAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var ResourceServer
     */
    private $resourceServer;

    private $failedException;

    public function __construct(ResourceServer $resourceServer)
    {
        $this->resourceServer = $resourceServer;
    }

    /**
     * {@inheritdoc}
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        if ($authException instanceof AuthenticationException) {
            throw $authException;
        }

        return new RedirectResponse('/login');
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(Request $request)
    {
        $psr7Factory = new DiactorosFactory();
        /** @var ServerRequestInterface $psrRequest */
        $psrRequest = $psr7Factory->createRequest($request);

        try {
            $psrRequest = $this->resourceServer->validateAuthenticatedRequest($psrRequest);
        } catch (\Exception $exception) {
            $this->failedException = $exception;

            if ($exception instanceof OAuthServerException && 401 === $exception->getHttpStatusCode()) {
                return;
            }

            return ['user_id' => $request->request->getInt('oauth_user_id')];
        }

        $uid = $psrRequest->getAttribute('oauth_user_id', false);
        $scopes = $psrRequest->getAttribute('oauth_scopes', false);
        if (!$uid) {
            return;
        }

        return [
            'user_id' => $uid,
            'scopes' => $scopes,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        // TODO: Implement getUser() method.
    }

    /**
     * {@inheritdoc}
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        // TODO: Implement checkCredentials() method.
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        // TODO: Implement onAuthenticationFailure() method.
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // TODO: Implement onAuthenticationSuccess() method.
    }

    /**
     * {@inheritdoc}
     */
    public function supportsRememberMe()
    {
        return false;
    }
}
