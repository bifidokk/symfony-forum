<?php

declare(strict_types=1);

namespace ForumBundle\Twig\Extension;

use ForumBundle\Paginator\Paginator;

class PaginatorExtension extends \Twig_Extension
{
    public function __construct()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('forum_paginator_render', [$this, 'render'], ['is_safe' => ['html'], 'needs_environment' => true]),
        ];
    }

    public function render(\Twig_Environment $env, Paginator $paginator)
    {
        return $env->render(
            'ForumBundle:Pagination:form.paginator.sliding.html.twig', [
                'paginator' => $paginator,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'forum_paginator';
    }
}
