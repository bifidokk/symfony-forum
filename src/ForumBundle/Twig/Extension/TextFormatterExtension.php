<?php

declare(strict_types=1);

namespace ForumBundle\Twig\Extension;

use s9e\TextFormatter\Bundles\Forum as TextFormatter;

class TextFormatterExtension extends \Twig_Extension
{
    public function __construct()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('text_formatter_unparse', [$this, 'unparse'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('text_formatter_render', [$this, 'render'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @param $post
     *
     * @return mixed|string
     */
    public function unparse($post)
    {
        return TextFormatter::unparse($post);
    }

    /**
     * @param $post
     *
     * @return mixed
     */
    public function render($post)
    {
        return TextFormatter::render($post);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'text_formatter';
    }
}
