<?php

declare(strict_types=1);

namespace ForumBundle;

use ForumBundle\DependencyInjection\Compiler\OverrideServiceTokenBasedRememberMe;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ForumBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new OverrideServiceTokenBasedRememberMe());
    }
}
