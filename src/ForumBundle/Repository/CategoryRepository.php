<?php

declare(strict_types=1);

namespace ForumBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class CategoryRepository extends EntityRepository
{
    public function findAllWithForums()
    {
        $em = $this->getEntityManager();
        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addRootEntityFromClassMetadata('ForumBundle:Category', 'c');

        $sql = 'SELECT c.*, COUNT(f.id) as countForums FROM category c
                 LEFT JOIN forum f ON(f.category_id = c.id)
                 GROUP BY c.id
                 HAVING countForums > 0';

        return $em->createNativeQuery($sql, $rsm)->getResult();
    }
}
