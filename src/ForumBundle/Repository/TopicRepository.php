<?php

declare(strict_types=1);

namespace ForumBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class TopicRepository extends EntityRepository
{
    public function getTopicAndPostCountForForumById($forumId)
    {
        if (null === $forumId || !\is_numeric($forumId) || 0 === $forumId) {
            throw new \Exception('Forum id "'.$forumId.'" is invalid!');
        }

        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT COUNT(DISTINCT p.id) as postCount,  COUNT(DISTINCT t.id) as topicCount
                                   FROM ForumBundle:Post p
                                   JOIN p.topic t
                                   WHERE t.forum = '".$forumId."'");

        try {
            return $query->getSingleResult();
        } catch (NoResultException $e) {
            return ['topicCount' => null, 'postCount' => null];
        } catch (\Exception $e) {
            return ['topicCount' => null, 'postCount' => null];
        }
    }

    public function findAllTopicsByForumId($forumId)
    {
        if (null === $forumId || !\is_numeric($forumId) || 0 === $forumId) {
            throw new \Exception('Forum id "'.$forumId.'" is invalid!');
        }

        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT t, p, u, fp, uu, tg FROM ForumBundle:Topic t
                                   LEFT JOIN t.lastPost p
                                   LEFT JOIN t.firstPost fp
                                   LEFT JOIN p.author u
                                   LEFT JOIN fp.author uu
                                   LEFT JOIN t.tags tg
                                   WHERE t.forum = '".$forumId."'
                                   ORDER BY t.isSticky DESC, t.lastPost DESC");

        return $query->getResult();
    }

    public function findAllTopicsByTagId($tagId)
    {
        if (null === $tagId || !\is_numeric($tagId) || 0 === $tagId) {
            throw new \Exception('Tag id "'.$tagId.'" is invalid!');
        }

        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT t, p, u, fp, uu, tg FROM ForumBundle:Topic t
                                   LEFT JOIN t.lastPost p
                                   LEFT JOIN t.firstPost fp
                                   LEFT JOIN p.author u
                                   LEFT JOIN fp.author uu
                                   LEFT JOIN t.tags tg
                                   WHERE tg.id = '".$tagId."'
                                   ORDER BY t.isSticky DESC, t.lastPost DESC");

        return $query->getResult();
    }

    public function getLastTopicForForumById($forumId)
    {
        if (null === $forumId || !\is_numeric($forumId) || 0 === $forumId) {
            throw new \Exception('Forum id "'.$forumId.'" is invalid!');
        }

        $em = $this->getEntityManager();
        $query = $em
            ->createQuery("SELECT t FROM ForumBundle:Topic t
                           WHERE t.forum = '".$forumId."'
                           ORDER BY t.created DESC")
            ->setMaxResults(1);

        return $query->getSingleResult();
    }
}
