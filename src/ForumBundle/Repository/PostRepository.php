<?php

declare(strict_types=1);

namespace ForumBundle\Repository;

use Doctrine\ORM\EntityRepository;
use ForumBundle\Entity\Post;

class PostRepository extends EntityRepository
{
    /**
     * @param $topicId int
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     *
     * @return int
     */
    public function countPostsForTopicById($topicId)
    {
        if (null === $topicId || !\is_numeric($topicId) || 0 === $topicId) {
            throw new \Exception('Topic id "'.$topicId.'" is invalid!');
        }

        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT COUNT(p.id) as total FROM ForumBundle:Post p
                                   WHERE p.topic = '".$topicId."'");

        $result = $query->getSingleResult();

        return (int) $result['total'];
    }

    /**
     * @param $topicId
     *
     * @throws \Exception
     *
     * @return array
     */
    public function getPostsForTopicById($topicId)
    {
        if (null === $topicId || !\is_numeric($topicId) || 0 === $topicId) {
            throw new \Exception('Topic id "'.$topicId.'" is invalid!');
        }

        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT p, u FROM ForumBundle:Post p
                                   LEFT JOIN p.author u
                                   WHERE p.topic = '".$topicId."'
                                   ORDER BY p.created");

        return $query->getResult();
    }

    /**
     * @param $topicId
     *
     * @throws \Exception
     *
     * @return \Doctrine\ORM\Query
     */
    public function getPostsQueryForTopicById($topicId)
    {
        if (null === $topicId || !\is_numeric($topicId) || 0 === $topicId) {
            throw new \Exception('Topic id "'.$topicId.'" is invalid!');
        }

        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT p, u FROM ForumBundle:Post p
                                   LEFT JOIN p.author u
                                   WHERE p.topic = '".$topicId."'
                                   ORDER BY p.created");

        return $query;
    }

    /**
     * @param $topicId
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     *
     * @return mixed
     */
    public function getLastPostForTopicById($topicId)
    {
        if (null === $topicId || !\is_numeric($topicId) || 0 === $topicId) {
            throw new \Exception('Topic id "'.$topicId.'" is invalid!');
        }

        $em = $this->getEntityManager();
        $query = $em
            ->createQuery("SELECT p FROM ForumBundle:Post p
                           WHERE p.topic = '".$topicId."'
                           ORDER BY p.created DESC")
            ->setMaxResults(1);

        return $query->getSingleResult();
    }

    /**
     * @param $topicId
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     *
     * @return mixed
     */
    public function getFirstPostForTopicById($topicId)
    {
        if (null === $topicId || !\is_numeric($topicId) || 0 === $topicId) {
            throw new \Exception('Topic id "'.$topicId.'" is invalid!');
        }

        $em = $this->getEntityManager();
        $query = $em
            ->createQuery("SELECT p FROM ForumBundle:Post p
                           WHERE p.topic = '".$topicId."'
                           ORDER BY p.created")
            ->setMaxResults(1);

        return $query->getSingleResult();
    }

    /**
     * @param $forumId
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     *
     * @return mixed
     */
    public function getLastPostForForumById($forumId)
    {
        if (null === $forumId || !\is_numeric($forumId) || 0 === $forumId) {
            throw new \Exception('Forum id "'.$forumId.'" is invalid!');
        }

        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT p FROM ForumBundle:Post p
                                   JOIN p.topic t
                                   WHERE t.forum = '".$forumId."'
                                   ORDER BY p.created DESC")
            ->setMaxResults(1);

        return $query->getSingleResult();
    }

    /**
     * @param Post $post
     *
     * @return mixed
     */
    public function getCountPostsBeforeCurrentPost(Post $post)
    {
        if (!$post instanceof Post) {
            throw new \InvalidArgumentException('Post is invalid!');
        }

        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT COUNT(p) as total FROM ForumBundle:Post p
                                        WHERE p.topic = '".$post->getTopic()->getId()."' AND 
                                        p.created <= '".$post->getCreated()->format('Y-m-d H:i:s')."'");

        return $query->getSingleResult();
    }
}
