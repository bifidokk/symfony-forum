<?php

declare(strict_types=1);

namespace ForumBundle\Repository\OAuth;

use Doctrine\ORM\EntityRepository;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;

class ClientRepository extends EntityRepository implements ClientRepositoryInterface
{
    const VALID_GRANTS = ['password', 'refresh_token'];

    /**
     * {@inheritdoc}
     */
    public function getClientEntity(
        $clientIdentifier,
        $grantType = null,
        $clientSecret = null,
        $mustValidateSecret = true
    ) {
        if (!\in_array($grantType, self::VALID_GRANTS, true)) {
            return;
        }

        $client = $this->find($clientIdentifier);
        if ($client instanceof ClientEntityInterface && $mustValidateSecret) {
            if ($client->getSecret() !== $clientSecret) {
                $client = null;
            }
        }

        return $client;
    }
}
