<?php

declare(strict_types=1);

namespace ForumBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ForumRepository extends EntityRepository
{
    public function getForumsWithCategories($id = 0)
    {
        $where = ($id > 0) ? " WHERE f.category = '".$id."'" : '';

        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT f, c, p, u FROM ForumBundle:Forum f
                                   JOIN f.category c
                                   LEFT JOIN f.lastPost p
                                   LEFT JOIN p.author u
                                   {$where}
                                   ORDER BY f.category");

        return $query->getResult();
    }
}
