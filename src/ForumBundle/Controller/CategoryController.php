<?php

declare(strict_types=1);

namespace ForumBundle\Controller;

use ForumBundle\Entity\Category;
use ForumBundle\Manager\CategoryManager;
use ForumBundle\Manager\ForumManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CategoryController extends Controller
{
    /**
     * @var CategoryManager
     */
    private $categoryManager;

    /**
     * @var ForumManager
     */
    private $forumManager;

    /**
     * @Route("/", name="forum_categories_list")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction()
    {
        $this->denyAccessUnlessGranted('view');

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Категории');

        $forums = $this->getForumManager()->getAllForumsWithCategories();

        return $this->render('ForumBundle:Category:list.html.twig', [
            'forums' => $forums,
        ]);
    }

    /**
     * @Route("/category/{id}", name="category_list")
     * @Method("GET")
     *
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showCategoryAction($id)
    {
        $this->denyAccessUnlessGranted('view');

        $breadcrumbs = $this->get('white_october_breadcrumbs');

        /**
         * @var Category
         */
        $category = $this->getCategoryManager()->getCategory($id);
        if (!$category) {
            throw $this->createNotFoundException();
        }

        $forums = $category->getForums();
        if (0 === $forums->count()) {
            throw $this->createNotFoundException();
        }

        $breadcrumbs->addItem($category->getName(), $this->get('router')->generate('category_list', ['id' => $id]));

        return $this->render('ForumBundle:Category:show.html.twig', [
            'category' => $category,
            'forums' => $forums,
        ]);
    }

    /**
     * @return CategoryManager
     */
    private function getCategoryManager()
    {
        if (!$this->categoryManager) {
            $this->categoryManager = $this->get('category_manager');
        }

        return $this->categoryManager;
    }

    /**
     * @return ForumManager
     */
    private function getForumManager()
    {
        if (!$this->forumManager) {
            $this->forumManager = $this->get('forum_manager');
        }

        return $this->forumManager;
    }
}
