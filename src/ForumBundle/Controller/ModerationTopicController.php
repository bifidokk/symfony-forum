<?php

declare(strict_types=1);

namespace ForumBundle\Controller;

use ForumBundle\Entity\Forum;
use ForumBundle\Entity\Topic;
use ForumBundle\Form\ChoiceList\Loader\TopicActionsChoice;
use ForumBundle\Form\Handler\TopicDeleteFormHandler;
use ForumBundle\Form\Type\TopicActionFormType;
use ForumBundle\Manager\TopicManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ModerationTopicController extends Controller
{
    /**
     * @var TopicManager
     */
    private $topicManager;

    /**
     * @Route("/topic/{id}/moderation", name="topic_moderation_action")
     * @Method("POST")
     *
     * @param Request $request
     * @param $id
     *
     * @return RedirectResponse
     */
    public function topicModerationAction(Request $request, $id)
    {
        /** @var TokenInterface $token */
        $token = $this->get('security.token_storage')->getToken();
        $user = $token->getUser();
        if (!$user) {
            throw new AccessDeniedException();
        }

        $topicManager = $this->getTopicManager();
        $topic = $topicManager->getTopicById($id);
        if (!$topic) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(TopicActionFormType::class, null, [
            'topic' => $topic,
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $action = $form->getData()['action'] ?? null;

            switch ($action) {
                case TopicActionsChoice::CHOICE_DELETE:
                    $url = $this->generateUrl('topic_delete', ['id' => $topic->getId()]);
                break;
                case TopicActionsChoice::CHOICE_STICKY:
                    $url = $this->generateUrl('topic_sticky', ['id' => $topic->getId()]);
                    break;
                case TopicActionsChoice::CHOICE_UNSTICKY:
                    $url = $this->generateUrl('topic_unsticky', ['id' => $topic->getId()]);
                    break;
                case TopicActionsChoice::CHOICE_CLOSE:
                    $url = $this->generateUrl('topic_close', ['id' => $topic->getId()]);
                    break;
                case TopicActionsChoice::CHOICE_REOPEN:
                    $url = $this->generateUrl('topic_reopen', ['id' => $topic->getId()]);
                    break;
                case TopicActionsChoice::CHOICE_MOVE:
                    $url = $this->generateUrl('topic_move', ['id' => $topic->getId()]);
                    break;
                default:
                    throw new \InvalidArgumentException((string) $form->getErrors(true, false));
            }

            return new RedirectResponse($url);
        }

        throw new \InvalidArgumentException((string) $form->getErrors(true, false));
    }

    /**
     * @Route("/topic/{id}/delete", name="topic_delete")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param $id
     *
     * @return Response
     */
    public function topicDeleteAction(Request $request, $id)
    {
        $topicManager = $this->getTopicManager();
        /** @var Topic $topic */
        $topic = $topicManager->getTopicById($id);
        if (!$topic) {
            throw $this->createNotFoundException();
        }
        /** @var Forum $forum */
        $forum = $topic->getForum();
        if (!$forum) {
            throw $this->createNotFoundException();
        }

        $this->denyAccessUnlessGranted('delete', $topic);
        /** @var TopicDeleteFormHandler $formHandler */
        $formHandler = $this->get('forum.form.handler.topic.delete');
        $formHandler->setTopic($topic);
        $formHandler->setRequest($request);

        if ($formHandler->process()) {
            $url = $this->generateUrl('show_forum', ['id' => $forum->getId()]);
            $response = new RedirectResponse($url);

            return $response;
        }

        $errors = $formHandler->getForm()->getErrors(true);

        return $this->render('ForumBundle:Topic:delete.html.twig', [
            'form' => $formHandler->getForm()->createView(),
            'topic' => $topic,
            'errors' => $errors,
        ]);
    }

    /**
     * @Route("/topic/{id}/sticky", name="topic_sticky")
     * @Method({"GET", "POST"})
     *
     * @param $id
     *
     * @return RedirectResponse
     */
    public function topicStickyAction($id)
    {
        $topicManager = $this->getTopicManager();
        /** @var Topic $topic */
        $topic = $topicManager->getTopicById($id);
        if (!$topic) {
            throw $this->createNotFoundException();
        }
        /** @var Forum $forum */
        $forum = $topic->getForum();
        if (!$forum) {
            throw $this->createNotFoundException();
        }

        $this->denyAccessUnlessGranted('sticky', $topic);
        $topicManager->stickyTopic($topic);

        $url = $this->generateUrl('show_forum', ['id' => $forum->getId()]);
        $response = new RedirectResponse($url);

        return $response;
    }

    /**
     * @Route("/topic/{id}/unsticky", name="topic_unsticky")
     * @Method({"GET", "POST"})
     *
     * @param $id
     *
     * @return RedirectResponse
     */
    public function topicUnstickyAction($id)
    {
        $topicManager = $this->getTopicManager();
        /** @var Topic $topic */
        $topic = $topicManager->getTopicById($id);
        if (!$topic) {
            throw $this->createNotFoundException();
        }
        /** @var Forum $forum */
        $forum = $topic->getForum();
        if (!$forum) {
            throw $this->createNotFoundException();
        }

        $this->denyAccessUnlessGranted('unsticky', $topic);
        $topicManager->unstickyTopic($topic);

        $url = $this->generateUrl('show_forum', ['id' => $forum->getId()]);
        $response = new RedirectResponse($url);

        return $response;
    }

    /**
     * @Route("/topic/{id}/close", name="topic_close")
     * @Method({"GET", "POST"})
     *
     * @param $id
     *
     * @return RedirectResponse
     */
    public function topicCloseAction($id)
    {
        $topicManager = $this->getTopicManager();
        /** @var Topic $topic */
        $topic = $topicManager->getTopicById($id);
        if (!$topic) {
            throw $this->createNotFoundException();
        }
        /** @var Forum $forum */
        $forum = $topic->getForum();
        if (!$forum) {
            throw $this->createNotFoundException();
        }

        $this->denyAccessUnlessGranted('close', $topic);
        $topicManager->closeTopic($topic);

        $url = $this->generateUrl('show_forum', ['id' => $forum->getId()]);
        $response = new RedirectResponse($url);

        return $response;
    }

    /**
     * @Route("/topic/{id}/reopen", name="topic_reopen")
     * @Method({"GET", "POST"})
     *
     * @param $id
     *
     * @return RedirectResponse
     */
    public function topicReopenAction($id)
    {
        $topicManager = $this->getTopicManager();
        /** @var Topic $topic */
        $topic = $topicManager->getTopicById($id);
        if (!$topic) {
            throw $this->createNotFoundException();
        }
        /** @var Forum $forum */
        $forum = $topic->getForum();
        if (!$forum) {
            throw $this->createNotFoundException();
        }

        $this->denyAccessUnlessGranted('reopen', $topic);
        $topicManager->reopenTopic($topic);

        $url = $this->generateUrl('show_forum', ['id' => $forum->getId()]);
        $response = new RedirectResponse($url);

        return $response;
    }

    /**
     * @Route("/topic/{id}/move", name="topic_move")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param $id
     *
     * @return RedirectResponse|Response
     */
    public function topicMoveAction(Request $request, $id)
    {
        $topicManager = $this->getTopicManager();
        /** @var Topic $topic */
        $topic = $topicManager->getTopicById($id);
        if (!$topic) {
            throw $this->createNotFoundException();
        }
        /** @var Forum $forum */
        $forum = $topic->getForum();
        if (!$forum) {
            throw $this->createNotFoundException();
        }

        $this->denyAccessUnlessGranted('move', $topic);

        $formHandler = $this->get('forum.form.handler.topic.move');
        $formHandler->setTopic($topic);
        $formHandler->setRequest($request);

        if ($formHandler->process()) {
            $url = $this->generateUrl('show_forum', ['id' => $forum->getId()]);
            $response = new RedirectResponse($url);

            return $response;
        }

        $errors = $formHandler->getForm()->getErrors(true);

        return $this->render('ForumBundle:Topic:move.html.twig', [
            'form' => $formHandler->getForm()->createView(),
            'topic' => $topic,
            'errors' => $errors,
        ]);
    }

    /**
     * @return TopicManager
     */
    private function getTopicManager()
    {
        if (!$this->topicManager) {
            $this->topicManager = $this->get('topic_manager');
        }

        return $this->topicManager;
    }
}
