<?php

declare(strict_types=1);

namespace ForumBundle\Controller;

use ForumBundle\DTO\UserEditData;
use ForumBundle\Form\Type\UserEditType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * @Route("/user", name="user")
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function userAction(Request $request)
    {
        $user = $this->getUser();
        $userEditData = UserEditData::createFromUser($user);

        $data = $this->get('forum.form.serializer')->serialize(UserEditType::class, $userEditData);

        return $this->render('ForumBundle:User:edit.html.twig', [
            //'form' => $form->createView()
        ]);
    }
}
