<?php

declare(strict_types=1);

namespace ForumBundle\Controller;

use ForumBundle\Entity\Forum;
use ForumBundle\Entity\Topic;
use ForumBundle\Manager\ForumManager;
use ForumBundle\Manager\TopicManager;
use ForumBundle\Paginator\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ForumController extends Controller
{
    /**
     * @var ForumManager
     */
    private $forumManager;

    /**
     * @var TopicManager
     */
    private $topicManager;

    /**
     * @Route("/forum/{id}", name="show_forum")
     * @Method("GET")
     *
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        /** @var Forum $forum* */
        $forum = $this->getForumManager()->getForum($id);
        if (!$forum) {
            throw $this->createNotFoundException();
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem(
            $forum->getCategory()->getName(),
            $this->get('router')->generate('category_list', ['id' => $forum->getCategory()->getId()])
        );

        $breadcrumbs->addItem($forum->getName());

        $topics = $this->getTopicManager()->findAllTopicsByForumId($forum->getId());
        /** @var Topic $topic * */
        foreach ($topics as $topic) {
            $paginator = new Paginator(
                $topic->getPostsAmount(),
                $this->generateUrl('show_topic', ['id' => $topic->getId()]),
                $this->getParameter('forum.topic.items_per_page'),
                $this->getParameter('forum.paginator.topic.page_range')
            );

            $topic->setPaginator($paginator);
        }

        return $this->render('ForumBundle:Forum:show.html.twig', [
            'forum' => $forum,
            'topics' => $topics,
        ]);
    }

    /**
     * @return ForumManager
     */
    private function getForumManager()
    {
        if (!$this->forumManager) {
            $this->forumManager = $this->get('forum_manager');
        }

        return $this->forumManager;
    }

    /**
     * @return TopicManager
     */
    private function getTopicManager()
    {
        if (!$this->topicManager) {
            $this->topicManager = $this->get('topic_manager');
        }

        return $this->topicManager;
    }
}
