<?php

declare(strict_types=1);

namespace ForumBundle\Controller;

use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Zend\Diactoros\Stream;

/**
 * @Route("/oauth")
 */
class OAuthController extends Controller
{
    /**
     * @Route("/access_token", methods={"POST"})

     *
     * @param ServerRequestInterface $request
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function accessTokenAction(ServerRequestInterface $request)
    {
        $httpFoundationFactory = new HttpFoundationFactory();
        $response = new \Zend\Diactoros\Response();

        /* @var \League\OAuth2\Server\AuthorizationServer $server */
        $server = $this->get('oauth_server');

        try {
            $response = $server->respondToAccessTokenRequest($request, $response);
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
        } catch (\Exception $exception) {
            $body = new Stream('php://temp', 'r+');
            $body->write($exception->getMessage());
            $response = $response->withStatus(500)->withBody($body);
        }

        return $httpFoundationFactory->createResponse($response);
    }
}
