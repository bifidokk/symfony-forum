<?php

declare(strict_types=1);

namespace ForumBundle\Controller;

use ForumBundle\Entity\Forum;
use ForumBundle\Entity\Post;
use ForumBundle\Entity\Topic;
use ForumBundle\Form\Handler\TopicUpdateFormHandler;
use ForumBundle\Manager\ForumManager;
use ForumBundle\Manager\PostManager;
use ForumBundle\Manager\TopicManager;
use s9e\TextFormatter\Bundles\Forum as TextFormatter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PostController extends Controller
{
    /**
     * @var PostManager
     */
    private $postManager;

    /**
     * @var TopicManager
     */
    private $topicManager;

    /**
     * @var ForumManager
     */
    private $forumManager;

    /**
     * @Route("/forum/{id}/create", name="create_post")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request, $id)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!$user) {
            throw new AccessDeniedException();
        }

        $forumManager = $this->getForumManager();
        /** @var Forum $forum */
        $forum = $forumManager->getForum($id);
        if (!$forum) {
            throw $this->createNotFoundException();
        }

        $postManager = $this->getPostManager();
        $post = $postManager->createPost();
        $this->denyAccessUnlessGranted('create', $post);

        $topicManager = $this->getTopicManager();
        $topic = $topicManager->createTopic();

        $formHandler = $this->get('forum.form.handler.topic.create');
        $formHandler->setTopic($topic);
        $formHandler->setRequest($request);

        if ($formHandler->process($forum, $user)) {
            $url = $this->get('topic.url_generator')->redirectResponseForTopicOnLastPostPage($topic);
            $response = new RedirectResponse($url);

            return $response;
        }

        $errors = $formHandler->getForm()->getErrors(true);

        return $this->render('ForumBundle:Post:create.html.twig', [
            'form' => $formHandler->getForm()->createView(),
            'forum' => $forum,
            'errors' => $errors,
        ]);
    }

    /**
     * @Route("/topic/{id}/reply", name="reply_post")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function replyAction(Request $request, $id)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!$user) {
            throw new AccessDeniedException();
        }

        $topicManager = $this->getTopicManager();
        /** @var Topic $topic */
        $topic = $topicManager->getTopicById($id);
        if (!$topic) {
            throw $this->createNotFoundException();
        }

        $postManager = $this->getPostManager();
        $post = $postManager->createPost();
        $this->denyAccessUnlessGranted('reply', $topic);

        $formHandler = $this->get('forum.form.handler.post.create');
        $formHandler->setPost($post);
        $formHandler->setRequest($request);

        if ($formHandler->process($topic, $user)) {
            $url = $this->get('topic.url_generator')->redirectResponseForTopicOnLastPostPage($topic);
            $response = new RedirectResponse($url);

            return $response;
        }

        $errors = $formHandler->getForm()->getErrors(true);

        return $this->render('ForumBundle:Post:reply.html.twig', [
            'form' => $formHandler->getForm()->createView(),
            'topic' => $topic,
            'errors' => $errors,
        ]);
    }

    /**
     * @Route("/topic/{id}/edit/{postId}", name="edit_post")
     * @Method({"GET", "POST"})
     *
     * @param $id
     * @param $postId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, $postId)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!$user) {
            throw new AccessDeniedException();
        }

        $topicManager = $this->getTopicManager();
        /** @var Topic $topic */
        $topic = $topicManager->getTopicById($id);
        if (!$topic) {
            throw $this->createNotFoundException();
        }

        $postManager = $this->getPostManager();
        $post = $postManager->getPostById($postId);
        if (!$post) {
            throw $this->createNotFoundException();
        }

        $this->denyAccessUnlessGranted('edit', $post);

        $formHandler = $this->getFormHandlerToEditPost($post);
        if ($formHandler->process()) {
            $url = $this->get('topic.url_generator')->redirectResponseForTopicOnPostPage($post);
            $response = new RedirectResponse($url);

            return $response;
        }

        $errors = $formHandler->getForm()->getErrors(true);

        return $this->render('ForumBundle:Post:edit.html.twig', [
            'form' => $formHandler->getForm()->createView(),
            'topic' => $topic,
            'post' => $post,
            'errors' => $errors,
            'isTopicEdited' => ($formHandler instanceof TopicUpdateFormHandler),
        ]);
    }

    /**
     * @Route("/post/delete/{postId}", name="delete_post")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param $postId
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, $postId)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!$user) {
            throw new AccessDeniedException();
        }

        $postManager = $this->getPostManager();
        $post = $postManager->getPostById($postId);
        if (!$post) {
            throw $this->createNotFoundException();
        }

        $topic = $post->getTopic();
        if (!$topic) {
            throw $this->createNotFoundException();
        }

        $this->denyAccessUnlessGranted('delete', $post);

        $formHandler = $this->get('forum.form.handler.post.delete');
        $formHandler->setPost($post);
        $formHandler->setRequest($request);

        if ($formHandler->process()) {
            $url = $this->get('topic.url_generator')->redirectResponseForTopicOnLastPostPage($topic);
            $response = new RedirectResponse($url);

            return $response;
        }

        $errors = $formHandler->getForm()->getErrors(true);

        return $this->render('ForumBundle:Post:delete.html.twig', [
            'form' => $formHandler->getForm()->createView(),
            'post' => $post,
            'errors' => $errors,
        ]);
    }

    /**
     * @return PostManager
     */
    private function getPostManager()
    {
        if (!$this->postManager) {
            $this->postManager = $this->get('post_manager');
        }

        return $this->postManager;
    }

    /**
     * @return TopicManager
     */
    private function getTopicManager()
    {
        if (!$this->topicManager) {
            $this->topicManager = $this->get('topic_manager');
        }

        return $this->topicManager;
    }

    /**
     * @return ForumManager
     */
    private function getForumManager()
    {
        if (!$this->forumManager) {
            $this->forumManager = $this->get('forum_manager');
        }

        return $this->forumManager;
    }

    /**
     * @param Post $post
     *
     * @return \ForumBundle\Form\Handler\PostUpdateFormHandler|object
     */
    private function getFormHandlerToEditPost(Post $post)
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        if ($post->getTopic()->getFirstPost()->getId() === $post->getId()) {
            $formHandler = $this->get('forum.form.handler.topic.update');
        } else {
            $formHandler = $this->get('forum.form.handler.post.update');
        }

        $post->setPost(TextFormatter::unparse($post->getPost()));
        $formHandler->setPost($post);
        $formHandler->setRequest($request);

        return $formHandler;
    }
}
