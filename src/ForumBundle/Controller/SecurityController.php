<?php

declare(strict_types=1);

namespace ForumBundle\Controller;

use ForumBundle\Entity\User;
use ForumBundle\Event\FormEvent;
use ForumBundle\Event\UserEvents;
use ForumBundle\Form\Type\ActivateAccountFormType;
use ForumBundle\Form\Type\LoginFormType;
use ForumBundle\Manager\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        $helper = $this->get('security.authentication_utils');

        $user = new User();
        $form = $this->createForm(LoginFormType::class, $user);
        $form->handleRequest($request);

        $error = $helper->getLastAuthenticationError();

        return $this->render('ForumBundle:Security:login.html.twig', [
            'form' => $form->createView(),
            'errors' => $error ? [$error] : null,
        ]);
    }

    /**
     * @Route("/login_check", name="security_login_check")
     */
    public function loginCheckAction()
    {
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
    }

    /**
     * @Route("/activate", name="activate")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return null|\Symfony\Component\HttpFoundation\Response
     */
    public function activateAccountAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user->getEnabled()) {
            return new RedirectResponse($this->generateUrl('forum_categories_list'));
        }

        $form = $this->createForm(ActivateAccountFormType::class);
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        /** @var UserManager */
        $userManager = $this->get('forum.user_manager');

        $form->setData($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(UserEvents::REGISTRATION_SUCCESS, $event);
            $response = $event->getResponse();
            $userManager->updateUser($user);

            if (null !== $response) {
                return $response;
            }
        }

        $errors = $form->getErrors(true);

        return $this->render('ForumBundle:Security:activateAccount.html.twig', [
            'form' => $form->createView(),
            'errors' => $errors,
        ]);
    }
}
