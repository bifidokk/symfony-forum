<?php

declare(strict_types=1);

namespace ForumBundle\Controller;

use ForumBundle\Event\FilterUserResponseEvent;
use ForumBundle\Event\FormEvent;
use ForumBundle\Event\UserEvents;
use ForumBundle\Form\Type\RegistrationFormType;
use ForumBundle\Manager\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;

class RegistrationController extends Controller
{
    /**
     * @Route("/register", name="registration")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $form = $this->createForm(RegistrationFormType::class);
        /** @var UserManager */
        $userManager = $this->get('forum.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $form->setData($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(UserEvents::REGISTRATION_SUCCESS, $event);

            $userManager->updateUser($user);
            $response = $event->getResponse();

            if (null === $response) {
                $url = $this->generateUrl('registration_confirmed');
                $response = new RedirectResponse($url);
            }

            return $response;
        }

        $errors = $form->getErrors(true);

        return $this->render('ForumBundle:Register:form.html.twig', [
            'form' => $form->createView(),
            'errors' => $errors,
        ]);
    }

    /**
     * @Route("/check_email", name="registration_check_email")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function checkEmailAction()
    {
        $email = $this->get('session')->get('user_send_confirmation_email/email');
        $this->get('session')->remove('user_send_confirmation_email/email');
        $user = $this->get('forum.user_manager')->findUserByEmail($email);

        if (null === $user) {
            throw new NotFoundHttpException(\sprintf('Пользователя с email "%s" не существует', $email));
        }

        return $this->render('ForumBundle:Register:checkEmail.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/register_confirm/{token}", name="registration_confirm")
     * @Method("GET")
     *
     * @param Request $request
     * @param $token
     *
     * @return RedirectResponse
     */
    public function confirmAction(Request $request, $token)
    {
        $userManager = $this->get('forum.user_manager');
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException('Некорректный ключ подтверждения');
        }

        $user->setEnabled(true);
        $user->setConfirmationToken(null);

        $userManager->updateUser($user);

        $url = $this->generateUrl('registration_confirmed');
        $response = new RedirectResponse($url);

        $event = new FilterUserResponseEvent($user, $request, $response);
        $dispatcher->dispatch(UserEvents::REGISTRATION_CONFIRMED, $event);

        return $response;
    }

    /**
     * @Route("/register_confirmed", name="registration_confirmed")
     * @Method("GET")
     */
    public function confirmedAction()
    {
        $user = $this->getUser();
        if (!\is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->render('ForumBundle:Register:confirmed.html.twig', [
            'user' => $user,
        ]);
    }
}
