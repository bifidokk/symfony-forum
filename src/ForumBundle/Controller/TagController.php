<?php

declare(strict_types=1);

namespace ForumBundle\Controller;

use ForumBundle\Entity\Topic;
use ForumBundle\Manager\TagManager;
use ForumBundle\Manager\TopicManager;
use ForumBundle\Paginator\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TagController extends Controller
{
    /**
     * @var TagManager
     */
    private $tagManager;

    /**
     * @var TopicManager
     */
    private $topicManager;

    /**
     * @Route("/tag/{id}", name="show_tag")
     * @Method("GET")
     *
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $tag = $this->getTagManager()->getTag($id);
        if (!$tag) {
            throw $this->createNotFoundException();
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem($tag->getName());

        $topics = $this->getTopicManager()->findAllTopicsByTagId($tag->getId());

        /** @var Topic $topic * */
        foreach ($topics as $topic) {
            $paginator = new Paginator(
                $topic->getPostsAmount(),
                $this->generateUrl('show_topic', ['id' => $topic->getId()]),
                $this->getParameter('forum.topic.items_per_page'),
                $this->getParameter('forum.paginator.topic.page_range')
            );

            $topic->setPaginator($paginator);
        }

        return $this->render('ForumBundle:Tag:show.html.twig', [
            'tag' => $tag,
            'topics' => $topics,
        ]);
    }

    /**
     * @return TagManager
     */
    private function getTagManager()
    {
        if (!$this->tagManager) {
            $this->tagManager = $this->get('tag_manager');
        }

        return $this->tagManager;
    }

    /**
     * @return TopicManager
     */
    private function getTopicManager()
    {
        if (!$this->topicManager) {
            $this->topicManager = $this->get('topic_manager');
        }

        return $this->topicManager;
    }
}
