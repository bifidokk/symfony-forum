<?php

declare(strict_types=1);

namespace ForumBundle\Controller;

use ForumBundle\Entity\Topic;
use ForumBundle\Form\Type\TopicActionFormType;
use ForumBundle\Manager\PostManager;
use ForumBundle\Manager\TopicManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TopicController extends Controller
{
    /**
     * @var TopicManager
     */
    private $topicManager;

    /**
     * @var PostManager
     */
    private $postManager;

    /**
     * @Route("/topic/{id}/{page}", name="show_topic", defaults={"page" = 1, "_fragment" = ""}, requirements={"page": "\d+"})
     * @Method("GET")
     *
     * @param $id
     * @param int $page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id, $page = 1)
    {
        $paginator = $this->get('knp_paginator');

        /** @var Topic $topic */
        $topic = $this->getTopicManager()->getTopicById($id);
        if (!$topic) {
            throw $this->createNotFoundException();
        }

        $forum = $topic->getForum();

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem(
            $forum->getCategory()->getName(),
            $this->get('router')->generate('category_list', ['id' => $forum->getCategory()->getId()])
        );

        $breadcrumbs->addItem(
            $forum->getName(),
            $this->get('router')->generate('show_forum', ['id' => $forum->getId()])
        );

        $breadcrumbs->addItem($topic->getTitleForBreadcrumbs());

        $postsQuery = $this->getPostManager()->getPostsQueryForTopicById($topic->getId());

        $pagination = $paginator->paginate(
            $postsQuery,
            $page,
            10
        );

        $topicActionForm = $this->createForm(TopicActionFormType::class, null, [
            'topic' => $topic,
        ]);

        return $this->render('ForumBundle:Topic:show.html.twig', [
            'topic' => $topic,
            'pagination' => $pagination,
            'post' => $this->getPostManager()->createPost(),
            'topic_actions_form' => $topicActionForm->createView(),
        ]);
    }

    /**
     * @return TopicManager
     */
    private function getTopicManager()
    {
        if (!$this->topicManager) {
            $this->topicManager = $this->get('topic_manager');
        }

        return $this->topicManager;
    }

    /**
     * @return PostManager
     */
    private function getPostManager()
    {
        if (!$this->postManager) {
            $this->postManager = $this->get('post_manager');
        }

        return $this->postManager;
    }
}
