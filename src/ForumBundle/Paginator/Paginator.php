<?php

declare(strict_types=1);

namespace ForumBundle\Paginator;

class Paginator
{
    private $itemsAmount;
    private $url;
    private $pageRange;
    private $itemsPerPage;
    private $pagesArray;
    private $pagesCount;

    public function __construct($itemsAmount, $url, $itemsPerPage = 10, $pageRange = 5)
    {
        if ($itemsAmount <= 0 || $itemsPerPage <= 0) {
            throw new \InvalidArgumentException('Paginator invalid argument');
        }

        $this->itemsAmount = $itemsAmount;
        $this->url = $url;
        $this->itemsPerPage = $itemsPerPage;
        $this->pageRange = $pageRange;

        $this->pagesCount = \ceil($this->itemsAmount / $this->itemsPerPage);
        $this->pagesArray = \range(1, $this->pagesCount);
    }

    /**
     * @return mixed
     */
    public function getItemsAmount()
    {
        return $this->itemsAmount;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return int
     */
    public function getPageRange(): int
    {
        return $this->pageRange;
    }

    /**
     * @return int
     */
    public function getItemsPerPage(): int
    {
        return $this->itemsPerPage;
    }

    /**
     * @return array
     */
    public function getPagesArray(): array
    {
        return $this->pagesArray;
    }

    /**
     * @return float
     */
    public function getPagesCount(): float
    {
        return $this->pagesCount;
    }
}
