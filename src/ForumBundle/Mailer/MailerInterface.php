<?php

declare(strict_types=1);

namespace ForumBundle\Mailer;

use Symfony\Component\Security\Core\User\UserInterface;

interface MailerInterface
{
    /**
     * Send an email to a user to confirm the account creation.
     *
     * @param UserInterface $user
     */
    public function sendConfirmationEmailMessage(UserInterface $user);
}
