<?php

declare(strict_types=1);

namespace ForumBundle\Mailer;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class Mailer implements MailerInterface
{
    protected $mailer;
    protected $templating;
    protected $router;
    protected $parameters;

    public function __construct($mailer, EngineInterface $templating, RouterInterface $router, array $parameters)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->parameters = $parameters;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function sendConfirmationEmailMessage(UserInterface $user)
    {
        $template = $this->parameters['confirmation_email_template'];
        $url = $this->router->generate(
            'registration_confirm',
            ['token' => $user->getConfirmationToken()],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        $rendered = $this->templating->render($template, [
            'user' => $user,
            'confirmationUrl' => $url,
        ]);

        $this->sendEmailMessage($rendered, $this->parameters['from_email'], $user->getEmail());
    }

    /**
     * @param $renderedTemplate
     * @param $fromEmail
     * @param $toEmail
     */
    protected function sendEmailMessage($renderedTemplate, $fromEmail, $toEmail)
    {
        $renderedLines = \explode("\n", \trim($renderedTemplate));
        $subject = $renderedLines[0];
        $body = \implode("\n", \array_slice($renderedLines, 1));

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail)
            ->setBody($body, 'text/html');

        $this->mailer->send($message);
    }
}
