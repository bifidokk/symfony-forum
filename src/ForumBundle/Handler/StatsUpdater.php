<?php

declare(strict_types=1);

namespace ForumBundle\Handler;

use ForumBundle\Entity\Forum;
use ForumBundle\Entity\Topic;
use ForumBundle\Manager\ForumManager;
use ForumBundle\Manager\PostManager;
use ForumBundle\Manager\TopicManager;

/**
 * Class StatsUpdater.
 */
class StatsUpdater
{
    /**
     * @var ForumManager
     */
    protected $forumManager;

    /**
     * @var TopicManager
     */
    protected $topicManager;

    /**
     * @var PostManager
     */
    protected $postManager;

    /**
     * @param ForumManager $forumManager
     * @param TopicManager $topicManager
     * @param PostManager  $postManager
     */
    public function __construct(ForumManager $forumManager, TopicManager $topicManager, PostManager $postManager)
    {
        $this->forumManager = $forumManager;
        $this->topicManager = $topicManager;
        $this->postManager = $postManager;
    }

    /**
     * @param Topic $topic
     */
    public function updateTopicStats(Topic $topic)
    {
        if ($topic->getId()) {
            $topicPostAmount = $this->postManager->countPostsForTopicById($topic->getId());
            $topic->setPostsAmount($topicPostAmount);

            $lastPost = $this->postManager->getLastPostForTopicById($topic->getId());
            $topic->setLastPost($lastPost ? $lastPost : null);

            $firstPost = $this->postManager->getFirstPostForTopicById($topic->getId());
            $topic->setFirstPost($firstPost ? $firstPost : null);

            $this->topicManager->updateTopic($topic);
        }
    }

    /**
     * @param Forum $forum
     */
    public function updateForumStats(Forum $forum)
    {
        if ($forum->getId()) {
            $stats = $this->topicManager->getTopicAndPostCountForForumById($forum->getId());
            $forum->setPostsAmount($stats['postCount']);
            $forum->setTopicsAmount($stats['topicCount']);

            $lastPost = $this->postManager->getLastPostForForumById($forum->getId());
            $forum->setLastPost($lastPost ? $lastPost : null);

            $this->forumManager->updateForum($forum);
        }
    }
}
