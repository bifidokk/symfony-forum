<?php

declare(strict_types=1);

namespace ForumBundle\Handler;

use ForumBundle\Entity\Post;
use ForumBundle\Entity\Topic;
use ForumBundle\Manager\PostManager;
use Symfony\Component\Routing\RouterInterface;

class TopicUrlGenerator
{
    private $router;
    private $postManager;
    private $postsPerPage;

    public function __construct(RouterInterface $router, PostManager $postManager, $postsPerPage)
    {
        $this->router = $router;
        $this->postManager = $postManager;
        $this->postsPerPage = $postsPerPage;
    }

    public function redirectResponseForTopicOnLastPostPage(Topic $topic)
    {
        return $this->router->generate('show_topic', [
            'id' => $topic->getId(),
            'page' => \ceil($topic->getPostsAmount() / $this->postsPerPage),
            '_fragment' => $topic->getLastPost()->getId(),
        ]);
    }

    public function redirectResponseForTopicOnPostPage(Post $post)
    {
        $result = $this->postManager->getCountPostsBeforeCurrentPost($post);
        $postsAmountBeforeCurrent = (int) $result['total'];

        return $this->router->generate('show_topic', [
            'id' => $post->getTopic()->getId(),
            'page' => \ceil($postsAmountBeforeCurrent / $this->postsPerPage),
            '_fragment' => $post->getId(),
        ]);
    }
}
