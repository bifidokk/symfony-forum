<?php

declare(strict_types=1);

namespace ForumBundle\Event;

use ForumBundle\Entity\Forum;
use ForumBundle\Entity\Topic;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

class UserTopicEvent extends Event
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * @var Topic
     */
    protected $topic;

    /**
     * @var Forum
     */
    protected $oldForum;

    /**
     * UserPostEvent constructor.
     *
     * @param Request    $request
     * @param Topic|null $topic
     */
    public function __construct(Request $request, Topic $topic)
    {
        $this->request = $request;
        $this->topic = $topic;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return Topic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @return Forum
     */
    public function getOldForum(): Forum
    {
        return $this->oldForum;
    }

    /**
     * @param Forum $oldForum
     */
    public function setOldForum(Forum $oldForum)
    {
        $this->oldForum = $oldForum;
    }
}
