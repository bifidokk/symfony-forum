<?php

declare(strict_types=1);

namespace ForumBundle\Event;

use ForumBundle\Entity\Post;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

class UserPostEvent extends Event
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * @var Post
     */
    protected $post;

    /**
     * UserPostEvent constructor.
     *
     * @param Request   $request
     * @param Post|null $post
     */
    public function __construct(Request $request, Post $post)
    {
        $this->request = $request;
        $this->post = $post;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return Post
     */
    public function getPost()
    {
        return $this->post;
    }
}
