<?php

declare(strict_types=1);

namespace ForumBundle\Event;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

class FilterUserResponseEvent extends UserEvent
{
    private $response;

    public function __construct(UserInterface $user, Request $request, Response $response = null)
    {
        parent::__construct($user, $request);
        $this->response = $response;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }
}
