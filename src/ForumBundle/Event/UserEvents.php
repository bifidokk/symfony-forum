<?php

declare(strict_types=1);

namespace ForumBundle\Event;

final class UserEvents
{
    /**
     * @Event("ForumBundle\Event\FormEvent")
     */
    const REGISTRATION_SUCCESS = 'user.registration.success';

    /**
     * @Event("ForumBundle\Event\FilterUserResponseEvent")
     */
    const REGISTRATION_CONFIRMED = 'user.registration.confirmed';

    /**
     * @Event("ForumBundle\Event\UserPostEvent")
     */
    const USER_POST_ADDED = 'user.post_added';

    /**
     * @Event("ForumBundle\Event\UserPostEvent")
     */
    const USER_POST_DELETED = 'user.post_deleted';

    /**
     * @Event("ForumBundle\Event\UserTopicEvent")
     */
    const USER_TOPIC_DELETED = 'user.topic_deleted';

    /**
     * @Event("ForumBundle\Event\UserTopicEvent")
     */
    const USER_TOPIC_MOVED = 'user.topic_moved';
}
