<?php

declare(strict_types=1);

namespace ForumBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ForumBundle\Paginator\Paginator;

/**
 * @ORM\Table(name="topic")
 * @ORM\Entity(repositoryClass="ForumBundle\Repository\TopicRepository")
 */
class Topic
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="Forum")
     * @ORM\JoinColumn(name="forum_id", referencedColumnName="id")
     */
    protected $forum;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(name="posts_amount", type="integer")
     */
    protected $postsAmount = 0;

    /**
     * @ORM\OneToOne(targetEntity="Post")
     * @ORM\JoinColumn(name="last_post", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $lastPost;

    /**
     * @ORM\OneToOne(targetEntity="Post", cascade={"persist"})
     * @ORM\JoinColumn(name="first_post", referencedColumnName="id")
     */
    protected $firstPost;

    /**
     * @ORM\Column(name="is_sticky", type="boolean", options={"default":0})
     */
    protected $isSticky = 0;

    /**
     * @ORM\Column(name="closed", type="boolean", options={"default":0})
     */
    protected $closed = 0;

    /**
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="tagTopics", cascade={"persist"})
     * @ORM\JoinTable(name="topic_tags")
     */
    protected $tags;

    /**
     * @var Paginator
     */
    private $paginator;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->tags = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getForum()
    {
        return $this->forum;
    }

    /**
     * @param mixed $forum
     */
    public function setForum($forum)
    {
        $this->forum = $forum;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPostsAmount()
    {
        return $this->postsAmount;
    }

    /**
     * @param mixed $postsAmount
     */
    public function setPostsAmount($postsAmount)
    {
        $this->postsAmount = $postsAmount;
    }

    /**
     * @return Post
     */
    public function getLastPost()
    {
        return $this->lastPost;
    }

    /**
     * @param Post $lastPost
     */
    public function setLastPost(Post $lastPost)
    {
        $this->lastPost = $lastPost;
    }

    /**
     * @return mixed
     */
    public function getFirstPost()
    {
        return $this->firstPost;
    }

    /**
     * @param mixed $firstPost
     */
    public function setFirstPost($firstPost)
    {
        $this->firstPost = $firstPost;
    }

    /**
     * @return mixed
     */
    public function getIsSticky()
    {
        return $this->isSticky;
    }

    /**
     * @param mixed $isSticky
     */
    public function setIsSticky($isSticky)
    {
        $this->isSticky = $isSticky;
    }

    /**
     * @return mixed
     */
    public function getClosed()
    {
        return $this->closed;
    }

    /**
     * @param mixed $closed
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;
    }

    /**
     * @return Paginator
     */
    public function getPaginator(): Paginator
    {
        return $this->paginator;
    }

    /**
     * @param Paginator $paginator
     */
    public function setPaginator(Paginator $paginator)
    {
        $this->paginator = $paginator;
    }

    /**
     * @return ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @param Tag $tag
     */
    public function addTag(Tag $tag)
    {
        $tag->addTopic($this);
        $this->tags->add($tag);
    }

    /**
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * @return string
     */
    public function getTitleForBreadcrumbs()
    {
        if (mb_strlen($this->title) <= 20) {
            return $this->title;
        } else {
            return mb_substr($this->title, 0, 20, 'utf8').'...';
        }
    }
}
