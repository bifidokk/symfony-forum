<?php

declare(strict_types=1);

namespace ForumBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="tag")
 * @ORM\Entity(repositoryClass="ForumBundle\Repository\TagRepository")
 */
class Tag
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="Topic", mappedBy="tags")
     */
    protected $tagTopics;

    public function __construct()
    {
        $this->tagTopics = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param Topic $topic
     */
    public function addTopic(Topic $topic)
    {
        if (!$this->tagTopics->contains($topic)) {
            $this->tagTopics->add($topic);
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getTopics()
    {
        return $this->tagTopics;
    }
}
