<?php

declare(strict_types=1);

namespace ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="forum")
 * @ORM\Entity(repositoryClass="ForumBundle\Repository\ForumRepository")
 */
class Forum
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="forums")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @ORM\Column(name="posts_amount", type="integer")
     */
    protected $postsAmount = 0;

    /**
     * @ORM\Column(name="topics_amount", type="integer")
     */
    protected $topicsAmount = 0;

    /**
     * @ORM\OneToOne(targetEntity="Post")
     * @ORM\JoinColumn(name="last_post", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $lastPost = null;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Forum
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set category.
     *
     * @param Category $category
     *
     * @return Forum
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return int
     */
    public function getPostsAmount()
    {
        return $this->postsAmount;
    }

    /**
     * @param int $postsAmount
     */
    public function setPostsAmount($postsAmount)
    {
        $this->postsAmount = $postsAmount;
    }

    /**
     * @return int
     */
    public function getTopicsAmount()
    {
        return $this->topicsAmount;
    }

    /**
     * @param int $topicsAmount
     */
    public function setTopicsAmount($topicsAmount)
    {
        $this->topicsAmount = $topicsAmount;
    }

    /**
     * @return mixed
     */
    public function getLastPost()
    {
        return $this->lastPost;
    }

    /**
     * @param mixed $lastPost
     */
    public function setLastPost($lastPost)
    {
        $this->lastPost = $lastPost;
    }
}
