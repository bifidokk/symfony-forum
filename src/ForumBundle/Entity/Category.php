<?php

declare(strict_types=1);

namespace ForumBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="ForumBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="Forum", mappedBy="category")
     */
    protected $forums;

    /**
     * @var int
     */
    protected $countForums = 0;

    public function __construct()
    {
        $this->forums = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getForums()
    {
        return $this->forums;
    }

    /**
     * @param mixed $forum
     */
    public function setForums($forum)
    {
        $this->forums[] = $forum;
    }

    /**
     * @return int
     */
    public function getCountForums()
    {
        return $this->countForums;
    }

    /**
     * @param int $countForums
     */
    public function setCountForums($countForums)
    {
        $this->countForums = $countForums;
    }
}
