<?php

declare(strict_types=1);

namespace ForumBundle\Entity\OAuth;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\Traits\AccessTokenTrait;
use League\OAuth2\Server\Entities\Traits\TokenEntityTrait;

/**
 * @ORM\Table(name="oauth_access_token", indexes={@Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity(repositoryClass="ForumBundle\Repository\OAuth\AccessTokenRepository")
 */
class AccessToken implements AccessTokenEntityInterface
{
    use AccessTokenTrait, TokenEntityTrait;
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="string", length=80)
     */
    protected $identifier;

    /**
     * @ORM\ManyToOne(targetEntity="ForumBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    protected $client;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $expired_at;

    /**
     * @ORM\Column(type="string")
     */
    protected $scopes;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @return mixed
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param mixed $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setClient(ClientEntityInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getExpiredAt()
    {
        return $this->expired_at;
    }

    /**
     * @param mixed $expired_at
     */
    public function setExpiredAt($expired_at)
    {
        $this->expired_at = $expired_at;
    }

    /**
     * @return mixed
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @param mixed $scopes
     */
    public function setScopes($scopes)
    {
        $this->scopes = $scopes;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \DateTime
     */
    public function getExpiryDateTime(): \DateTime
    {
        return $this->getExpiredAt();
    }

    /**
     * @param \DateTime $expiryDateTime
     */
    public function setExpiryDateTime(\DateTime $expiryDateTime)
    {
        $this->setExpiredAt($expiryDateTime);
    }
}
