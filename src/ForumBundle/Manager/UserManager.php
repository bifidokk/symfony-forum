<?php

declare(strict_types=1);

namespace ForumBundle\Manager;

use Doctrine\Common\Persistence\ObjectManager;
use ForumBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserManager
{
    protected $class;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var EncoderFactoryInterface
     */
    protected $encoderFactory;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    protected $repository;

    /**
     * UserManager constructor.
     *
     * @param ObjectManager           $om
     * @param EncoderFactoryInterface $encoderFactory
     * @param $class
     */
    public function __construct(ObjectManager $om, EncoderFactoryInterface $encoderFactory, $class)
    {
        $this->class = $class;
        $this->objectManager = $om;
        $this->encoderFactory = $encoderFactory;
        $this->repository = $om->getRepository($class);
    }

    /**
     * @return mixed
     */
    public function createUser()
    {
        $user = new $this->class();

        return $user;
    }

    /**
     * @param $email
     *
     * @return null|object
     */
    public function findUserByEmail($email)
    {
        return $this->findUserBy(['email' => $email]);
    }

    /**
     * @param $token
     *
     * @return null|object
     */
    public function findUserByConfirmationToken($token)
    {
        return $this->findUserBy(['confirmationToken' => $token]);
    }

    /**
     * @param array $criteria
     *
     * @return null|object
     */
    public function findUserBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param $user
     * @param bool $andFlush
     */
    public function updateUser(User $user, $andFlush = true)
    {
        $this->updatePassword($user);

        $this->objectManager->persist($user);
        if ($andFlush) {
            $this->objectManager->flush();
        }
    }

    public function updatePassword(User $user)
    {
        if (0 !== \strlen($password = $user->getPlainPassword())) {
            $encoder = $this->getEncoder($user);
            $user->setPassword($encoder->encodePassword($password, $user->getSalt()));
            $user->eraseCredentials();
        }
    }

    /**
     * @param UserInterface $user
     *
     * @return \Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface
     */
    protected function getEncoder(UserInterface $user)
    {
        return $this->encoderFactory->getEncoder($user);
    }
}
