<?php

declare(strict_types=1);

namespace ForumBundle\Manager;

use Doctrine\ORM\EntityManager;
use ForumBundle\Entity\Tag;

class TagManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $tagId
     *
     * @return Tag|null|object
     */
    public function getTag($tagId)
    {
        return $this->em->getRepository('ForumBundle:Tag')->find($tagId);
    }

    public function saveTag(Tag $tag)
    {
        $this->em->persist($tag);

        return $this;
    }
}
