<?php

declare(strict_types=1);

namespace ForumBundle\Manager;

use Doctrine\ORM\EntityManager;
use ForumBundle\Entity\Topic;

class TopicManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return Topic
     */
    public function createTopic()
    {
        $topic = new Topic();

        return $topic;
    }

    /**
     * @param $topicId
     *
     * @return null|object
     */
    public function getTopicById($topicId)
    {
        return $this->em->getRepository('ForumBundle:Topic')->find($topicId);
    }

    /**
     * @param $forumId int
     *
     * @return mixed
     */
    public function getTopicAndPostCountForForumById($forumId)
    {
        return $this->em->getRepository('ForumBundle:Topic')->getTopicAndPostCountForForumById($forumId);
    }

    /**
     * @param $forumId
     *
     * @return mixed
     */
    public function findAllTopicsByForumId($forumId)
    {
        return $this->em->getRepository('ForumBundle:Topic')->findAllTopicsByForumId($forumId);
    }

    /**
     * @param $tagId
     *
     * @return array
     */
    public function findAllTopicsByTagId($tagId)
    {
        return $this->em->getRepository('ForumBundle:Topic')->findAllTopicsByTagId($tagId);
    }

    /**
     * @param $forumId
     *
     * @return mixed
     */
    public function getLastTopicForForumById($forumId)
    {
        return $this->em->getRepository('ForumBundle:Topic')->getLastTopicForForumById($forumId);
    }

    /**
     * @param Topic $topic
     * @param bool  $forceFlush
     *
     * @return \ForumBundle\Manager\TopicManager
     */
    public function updateTopic(Topic $topic, $forceFlush = true)
    {
        $this->em->persist($topic);

        if (true === $forceFlush) {
            $this->em->flush();
        }

        return $this;
    }

    /**
     * @param Topic $topic
     *
     * @return \ForumBundle\Manager\TopicManager
     */
    public function saveTopic(Topic $topic)
    {
        $this->em->persist($topic);
        $this->em->flush();

        return $this;
    }

    /**
     * @param Topic $topic
     */
    public function deleteTopic(Topic $topic)
    {
        $this->em->remove($topic);
        $this->em->flush();
    }

    /**
     * @param Topic $topic
     *
     * @return TopicManager
     */
    public function stickyTopic(Topic $topic)
    {
        $topic->setIsSticky(true);

        return $this->saveTopic($topic);
    }

    /**
     * @param Topic $topic
     *
     * @return TopicManager
     */
    public function unstickyTopic(Topic $topic)
    {
        $topic->setIsSticky(false);

        return $this->saveTopic($topic);
    }

    /**
     * @param Topic $topic
     *
     * @return TopicManager
     */
    public function closeTopic(Topic $topic)
    {
        $topic->setClosed(true);

        return $this->saveTopic($topic);
    }

    /**
     * @param Topic $topic
     *
     * @return TopicManager
     */
    public function reopenTopic(Topic $topic)
    {
        $topic->setIsSticky(false);

        return $this->saveTopic($topic);
    }
}
