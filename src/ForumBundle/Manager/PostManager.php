<?php

declare(strict_types=1);

namespace ForumBundle\Manager;

use Doctrine\ORM\EntityManager;
use ForumBundle\Entity\Post;

class PostManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return Post
     */
    public function createPost()
    {
        $post = new Post();

        return $post;
    }

    /**
     * @param Post $post
     */
    public function savePost(Post $post)
    {
        $this->em->persist($post);
        $this->em->flush($post);
    }

    /**
     * @param $topicId int
     *
     * @return mixed
     */
    public function countPostsForTopicById($topicId)
    {
        return $this->em->getRepository('ForumBundle:Post')->countPostsForTopicById($topicId);
    }

    /**
     * @param $topicId
     *
     * @return mixed
     */
    public function getPostsForTopicById($topicId)
    {
        return $this->em->getRepository('ForumBundle:Post')->getPostsForTopicById($topicId);
    }

    /**
     * @param $topicId
     *
     * @return \Doctrine\ORM\Query
     */
    public function getPostsQueryForTopicById($topicId)
    {
        return $this->em->getRepository('ForumBundle:Post')->getPostsQueryForTopicById($topicId);
    }

    /**
     * @param $topicId
     *
     * @return mixed
     */
    public function getLastPostForTopicById($topicId)
    {
        return $this->em->getRepository('ForumBundle:Post')->getLastPostForTopicById($topicId);
    }

    /**
     * @param $topicId
     *
     * @return mixed
     */
    public function getFirstPostForTopicById($topicId)
    {
        return $this->em->getRepository('ForumBundle:Post')->getFirstPostForTopicById($topicId);
    }

    /**
     * @param $forumId
     *
     * @return mixed
     */
    public function getLastPostForForumById($forumId)
    {
        return $this->em->getRepository('ForumBundle:Post')->getLastPostForForumById($forumId);
    }

    /**
     * @param $postId
     *
     * @return Post|null|object
     */
    public function getPostById($postId)
    {
        return $this->em->getRepository('ForumBundle:Post')->find($postId);
    }

    /**
     * @param Post $post
     *
     * @return mixed
     */
    public function getCountPostsBeforeCurrentPost(Post $post)
    {
        return $this->em->getRepository('ForumBundle:Post')->getCountPostsBeforeCurrentPost($post);
    }

    /**
     * @param Post $post
     */
    public function deletePost(Post $post)
    {
        $this->em->remove($post);
        $this->em->flush();
    }
}
