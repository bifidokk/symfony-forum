<?php

declare(strict_types=1);

namespace ForumBundle\Manager;

use Doctrine\ORM\EntityManager;
use ForumBundle\Entity\Forum;

class ForumManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param int $id
     *
     * @return array
     */
    public function getAllForumsWithCategories($id = 0)
    {
        return $this->em->getRepository('ForumBundle:Forum')->getForumsWithCategories($id);
    }

    /**
     * @param $forumId
     *
     * @return null|object
     */
    public function getForum($forumId)
    {
        return $this->em->getRepository('ForumBundle:Forum')->find($forumId);
    }

    /**
     * @param Forum $forum
     *
     * @return \ForumBundle\Manager\ForumManager
     */
    public function updateForum(Forum $forum)
    {
        $this->em->persist($forum);
        $this->em->flush();

        return $this;
    }

    /**
     * @param Forum $forum
     *
     * @return \ForumBundle\Manager\ForumManager
     */
    public function saveForum(Forum $forum)
    {
        $this->em->persist($forum);
        $this->em->flush();

        return $this;
    }
}
