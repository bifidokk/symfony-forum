<?php

declare(strict_types=1);

namespace ForumBundle\Manager;

use Doctrine\ORM\EntityManager;

class CategoryManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $id
     *
     * @return null|object
     */
    public function getCategory($id)
    {
        return $this->em->getRepository('ForumBundle:Category')->find($id);
    }

    /**
     * @return array
     */
    public function findAllWithForums()
    {
        return $this->em->getRepository('ForumBundle:Category')->findAllWithForums();
    }
}
