<?php

declare(strict_types=1);

namespace ForumBundle\Manager;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\RememberMe\RememberMeServicesInterface;
use Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface;

class LoginManager
{
    private $tokenStorage;
    private $sessionStrategy;
    private $container;

    public function __construct(TokenStorageInterface $tokenStorage, SessionAuthenticationStrategyInterface $sessionStrategy, ContainerInterface $container)
    {
        $this->tokenStorage = $tokenStorage;
        $this->sessionStrategy = $sessionStrategy;
        $this->container = $container;
    }

    public function logInUser($firewallName, UserInterface $user, Request $request, Response $response = null)
    {
        $token = $this->createToken($user, $firewallName);

        if (null !== $request) {
            $this->sessionStrategy->onAuthentication($request, $token);

            $rememberMeServices = null;
            if ($this->container->has('my.authentication.rememberme.services.simplehash')) {
                $rememberMeServices = $this->container->get('my.authentication.rememberme.services.simplehash');
            }

            if ($rememberMeServices instanceof RememberMeServicesInterface) {
                $rememberMeServices->loginSuccess($request, $response, $token);
            }
        }

        $this->tokenStorage->setToken($token);
    }

    private function createToken(UserInterface $user, $firewallName)
    {
        return new UsernamePasswordToken($user, null, $firewallName, $user->getRoles());
    }
}
