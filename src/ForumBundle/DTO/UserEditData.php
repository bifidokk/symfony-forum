<?php

declare(strict_types=1);

namespace ForumBundle\DTO;

use ForumBundle\Entity\User;

class UserEditData
{
    private $name;
    private $action;

    /**
     * @param User $user
     *
     * @return static
     */
    public static function createFromUser(User $user)
    {
        $userEditData = new static();
        $userEditData->setName($user->getName());

        return $userEditData;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return UserEditData
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     *
     * @return UserEditData
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }
}
