<?php

declare(strict_types=1);

namespace ForumBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ForumBundle\Handler\StatsUpdater;
use Nelmio\Alice\Fixtures;

class LoadFixtures implements FixtureInterface
{
    /**
     * @var StatsUpdater
     */
    private $statsUpdater;

    public function __construct(StatsUpdater $statsUpdater)
    {
        $this->statsUpdater = $statsUpdater;
    }

    public function load(ObjectManager $manager)
    {
        $objects = Fixtures::load(
            __DIR__.'/fixtures.yml',
            $manager,
            [
                'providers' => [$this],
            ],
            [
                new PostProcessor($this->statsUpdater),
            ]
        );
    }
}
