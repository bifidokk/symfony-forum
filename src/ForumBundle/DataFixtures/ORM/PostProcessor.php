<?php

declare(strict_types=1);

namespace ForumBundle\DataFixtures\ORM;

use ForumBundle\Entity\Forum;
use ForumBundle\Entity\Post;
use ForumBundle\Entity\Topic;
use ForumBundle\Handler\StatsUpdater;
use Nelmio\Alice\ProcessorInterface;

class PostProcessor implements ProcessorInterface
{
    /**
     * @var StatsUpdater;
     */
    private $statsUpdaterHandler;

    public function __construct(StatsUpdater $statsUpdaterHandler)
    {
        $this->statsUpdaterHandler = $statsUpdaterHandler;
    }

    public function preProcess($object)
    {
    }

    public function postProcess($object)
    {
        if ($object instanceof Post) {
            /**
             * @var Topic
             */
            $topic = $object->getTopic();
            $this->statsUpdaterHandler->updateTopicStats($topic);

            /**
             * @var Forum
             */
            $forum = $topic->getForum();
            $this->statsUpdaterHandler->updateForumStats($forum);
        }
    }
}
