<?php

declare(strict_types=1);

namespace ForumBundle\EventListener;

use ForumBundle\Entity\Forum;
use ForumBundle\Entity\Post;
use ForumBundle\Entity\Topic;
use ForumBundle\Event\UserEvents;
use ForumBundle\Event\UserPostEvent;
use ForumBundle\Event\UserTopicEvent;
use ForumBundle\Handler\StatsUpdater;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ForumStatsUpdateListener implements EventSubscriberInterface
{
    private $statsUpdater;

    public function __construct(StatsUpdater $statsUpdater)
    {
        $this->statsUpdater = $statsUpdater;
    }

    public static function getSubscribedEvents()
    {
        return [
            UserEvents::USER_POST_ADDED => 'onPostAdded',
            UserEvents::USER_POST_DELETED => 'onPostDeleted',
            UserEvents::USER_TOPIC_DELETED => 'onTopicDeleted',
            UserEvents::USER_TOPIC_MOVED => 'onTopicMoved',
        ];
    }

    /**
     * @param UserPostEvent $event
     */
    public function onPostAdded(UserPostEvent $event)
    {
        $post = $event->getPost();
        if (!$post || !$post instanceof Post) {
            return;
        }

        /** @var Topic $topic */
        $topic = $post->getTopic();
        if (!$topic) {
            return;
        }

        /** @var Forum $forum */
        $forum = $topic->getForum();
        if (!$forum) {
            return;
        }

        $this->statsUpdater->updateTopicStats($topic);
        $this->statsUpdater->updateForumStats($forum);
    }

    /**
     * @param UserTopicEvent $event
     */
    public function onPostDeleted(UserTopicEvent $event)
    {
        /** @var Topic $topic */
        $topic = $event->getTopic();
        if (!$topic) {
            return;
        }

        /** @var Forum $forum */
        $forum = $topic->getForum();
        if (!$forum) {
            return;
        }

        $this->statsUpdater->updateTopicStats($topic);
        $this->statsUpdater->updateForumStats($forum);
    }

    /**
     * @param UserTopicEvent $event
     */
    public function onTopicDeleted(UserTopicEvent $event)
    {
        /** @var Topic $topic */
        $topic = $event->getTopic();
        if (!$topic) {
            return;
        }

        /** @var Forum $forum */
        $forum = $topic->getForum();
        if (!$forum) {
            return;
        }

        $this->statsUpdater->updateTopicStats($topic);
        $this->statsUpdater->updateForumStats($forum);
    }

    /**
     * @param UserTopicEvent $event
     */
    public function onTopicMoved(UserTopicEvent $event)
    {
        /** @var Topic $topic */
        $topic = $event->getTopic();
        if (!$topic) {
            return;
        }

        /** @var Forum $forum */
        $forum = $topic->getForum();
        if (!$forum) {
            return;
        }

        $this->statsUpdater->updateTopicStats($topic);
        $this->statsUpdater->updateForumStats($forum);

        /** @var Forum $oldForum */
        $oldForum = $event->getOldForum();
        if (!$oldForum) {
            return;
        }

        $this->statsUpdater->updateForumStats($oldForum);
    }
}
