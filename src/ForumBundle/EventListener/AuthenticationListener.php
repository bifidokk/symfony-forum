<?php

declare(strict_types=1);

namespace ForumBundle\EventListener;

use ForumBundle\Event\FilterUserResponseEvent;
use ForumBundle\Event\UserEvents;
use ForumBundle\Manager\LoginManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Exception\AccountStatusException;

class AuthenticationListener implements EventSubscriberInterface
{
    private $loginManager;
    private $firewallName;

    public function __construct(LoginManager $loginManager, $firewallName)
    {
        $this->loginManager = $loginManager;
        $this->firewallName = $firewallName;
    }

    public static function getSubscribedEvents()
    {
        return [
            UserEvents::REGISTRATION_CONFIRMED => 'authenticate',
        ];
    }

    public function authenticate(FilterUserResponseEvent $event)
    {
        if (!$event->getUser()->getEnabled()) {
            return;
        }

        try {
            $this->loginManager->logInUser($this->firewallName, $event->getUser(), $event->getRequest(), $event->getResponse());
        } catch (AccountStatusException $ex) {
        }
    }
}
